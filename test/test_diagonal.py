

def test_diag():
    """Test diagonal of adjacency matrix"""
    from chillo.io_pickle import load, save
    from chillo.snp_graph import SnpGraph
    import subprocess

    data = SnpGraph('test_example')
    data.read_pairs_csv('test/tiny_gwis.txt')
    print(data.diagonal)
    print(data.diagonal_unweighted)
    
if __name__ == '__main__':
    test_diag()
    
