"""Interface with various outputs of GWIS:
- the matlab GSS code
- the tuples from gwis-platform
"""

import os
import csv
import numpy
from numpy import genfromtxt, unique, zeros, empty

def read_gss_file(filename):
    """Parse the file output from matlab GSS code, and return an object"""
    raw_data = open(filename, 'r')
    raw_data.next() # Ignore first line
    second_line = raw_data.next()
    raw_data.close()
    (info_start, format_row, header_row, data_start, dummy) = map(int, second_line.split())
    format_list, header_list = get_header(filename, format_row-1, header_row-1)
    data = genfromtxt(filename, delimiter='\t', skip_header=data_start-1,
                      dtype={'names': header_list, 'formats': format_list})
    return data


def read_gwis_file(filename, score_col=2, min_score=1.0, max_pairs=1000000, line_start=4, delim='\t'):
    """Load the pairs and scores from the GWIS file format,
    returns a structured numpy array
    """
    format_list, header_list = get_header(filename, 2, 3)
    assert(header_list[0] == 'prb_1')
    assert(header_list[1] == 'prb_2')
    header_list[0] = 'prb_1'
    header_list[1] = 'prb_2'
    if header_list[2] != 'Chi2':
        header_list[2] = header_list[2][3:]
    all_pairs = empty(max_pairs, dtype={'names': header_list, 'formats': format_list})

    csvfile = open(filename, 'r')
    reader = csv.reader(csvfile, delimiter=delim) 
    for i, line in enumerate(reader):
        if i < line_start: continue  # skip over header
        ix=i-line_start
        if ix >= max_pairs: break
        if score_col == 0:
            all_pairs[ix]['prb_1'] = int(line[1])
            all_pairs[ix]['prb_2'] = int(line[2])
            all_pairs[ix][2] = float(line[0])
        elif score_col==2:
            all_pairs[ix]['prb_1'] = int(line[0])
            all_pairs[ix]['prb_2'] = int(line[1])
            all_pairs[ix][2] = float(line[2])
        if all_pairs[ix][2] < min_score: break

    csvfile.close()
    if ix < max_pairs:
        all_pairs = all_pairs[:ix]
    
    # Make edges positive
    if max(all_pairs[2]) < 1e-3:
        all_pairs[2] = -all_pairs[2]
    
    return all_pairs

def read_table_file(filename):
    """Parse the file output from matlab GSS code, and return an object"""
    raw_data = open(filename, 'r')
    raw_data.next() # Ignore first line
    second_line = raw_data.next()
    raw_data.close()
    (info_start, format_row, header_row, data_start, dummy) = map(int, second_line.split())
    format_list, header_list = get_header(filename, format_row-1, header_row-1)
    data = genfromtxt(filename, delimiter='\t', skip_header=data_start-1,
                      dtype={'names': header_list, 'formats': format_list})
    return data

def get_header(filename, format_row, header_row):
    """Parse the format and header rows from the file filename"""
    format2dtype = {'%s': 'S20', '%d': int, '%f': float}
    fp = open(filename, 'r')
    for ix, line in enumerate(fp):
        if ix > max(format_row, header_row):
            break
        elif ix == format_row:
            format_list = []
            for item in line.split():
                format_list.append(format2dtype[item])
        elif ix == header_row:
            header_list = line.split()

    # Hack because the two probes have the same title
    header_list[0] = 'prb_1'
    header_list[1] = 'prb_2'
    return format_list, header_list


# Functions for extracting unique SNPS as probe IDs from arrays created from GWIS input files
# function not tested
def all_snps_from_pairs(all_pairs):
    """ Input is a list of 3-column pair+value read from GWIS output format (read_gwis_file)
        Returns a list of snps as probe IDs from all_pairs"""
    all_snps=unique(all_pairs['snp1'].tolist()+all_pairs['snp2'].tolist())
    return all_snps

def all_snps_from_array(input_data):
    """ Input is ndarray from file read from matlab GSS code (see read_table_file) 
        Returns a list of snps as probe IDs"""
    all_snps=unique(input_data['prb_1'].tolist()+input_data['prb_2'].tolist())
    return all_snps

def init_snps_array(probe_list, bim_file):
    """Create a structured array with probe information.
    probe_list is a list of probe indices
    bim_file contains chrom, probe code and bp position
    assumes bim file index is probe_id-1 (ie at line 0, are the details for probe_id 1)
    returns a numpy structured array
    """
    all_snps = numpy.genfromtxt(open(bim_file, 'rb'), usecols=(0,1,3),
                            dtype={'names': ['chrom','prbCode','bp_position'],
                                   'formats':['S16', 'S16', int]})
    
    snp_array = zeros(len(probe_list), #initialize with zeros
        dtype={'names':['chrom','bp_position','rs','prbCode','prb'],
               'formats':[int,int,'S16','S16',int]})

    for idx,prb_id in enumerate(probe_list):
        snp_bim = all_snps[prb_id-1] # Assume probe ID starts from number 1
        snp_array[idx]['prb']=prb_id
        snp_array[idx]['prbCode']=snp_bim['prbCode']
        snp_array[idx]['rs'] = snp_bim['prbCode'] # No rs number in WTCCC file
        snp_array[idx]['chrom'] = snp_bim['chrom']
        snp_array[idx]['bp_position'] = snp_bim['bp_position']
    return snp_array

# functions fo extracting snp array information from all_snps
def create_snps_array(probe_list,bim_file,rs_location_file,rs_file_cols):
    """ probe_list is a list of probe_ids
        bim file contains chrom,probe code, and bp position
        rs location file contains updated location details
        rs_file_cols specifies chrom, bp_position, probe code, and rs identifier columns in rs_location_file
        assumes bim file index is probe_id-1 (ie at line 0, are the details for probe_id 1
        bim_file must contain a "probe code" in column 1 that matches to probe code column in rs_location file 
        (specified in rs_file_cols)
        """
    all_snps = numpy.genfromtxt(open(bim_file, 'rb'), usecols=(0,1,3),
                            dtype={'names': ['chrom','prbCode','bp_position'],
                                   'formats':['S16', 'S16', int]})
    
    if os.path.isfile(rs_location_file):  
        snp_location = numpy.genfromtxt(open(rs_location_file, 'rb'),                   
                     usecols=rs_file_cols, 
                     dtype={'names': ['chrom','bp_position','prbCode','rs'], 
                    'formats':['S16',int,'S16','S16']}) 

    snp_array = zeros(len(probe_list), #initialize with zeros
        dtype={'names':['chrom','bp_position','rs','prbCode','prb'],
               'formats':[int,int,'S16','S16',int]})

    # identify probes with SNP probe codes that are missing from rs_location_file

    idx_not_found=[]  # list to store idx of snps that are not found in probe_list
    for idx,prb_id in enumerate(probe_list):
        snp_bim=all_snps[prb_id-1]# details from bim file. Important, assume probe ID starts from number 1.#check adams file
        prbCode=snp_bim['prbCode'] #  
        rs_row=snp_location[(snp_location['prbCode']==prbCode),:]
        if len(rs_row) >1:
            print('RS location file contains more than one entry for %s' % prbCode) 
            break
        if len(rs_row)==0:  # no match found for prbCode in rs_location_file 
            print('No match found for %d %s' % (idx,prbCode))
            idx_not_found.append(idx)
        else:
            snp_array[idx]['prb']=prb_id # reset in case not correct, but looks ok              
            snp_array[idx]['prbCode']=prbCode # reset in case not correct, but looks ok              
            snp_array[idx]['rs'] = rs_row['rs']
            snp_array[idx]['chrom'] = rs_row['chrom'][0].split('chr')[1]
            snp_array[idx]['bp_position'] = rs_row['bp_position']
    print('Number of snps not found is %d'  % len(idx_not_found))

    # remove unwanted probes from snp_array
    filtered_snp_array=numpy.delete(snp_array,idx_not_found,axis=0)
    probe_not_found=probe_list[idx_not_found]

    return filtered_snp_array,probe_not_found


#function for updating GWIS all_pairs data using SNPS in SNPs array
#def update_all_pairs(all_pairs,snps_array):


def update_array(disease_array, probe_not_found):
    """ removing rows which contain probe IDs listed in probe_not_found"""
    idx_to_remove=[]
    for idx,probe in enumerate(probe_not_found):
        idx_data1=numpy.argwhere(disease_array['prb_1']==probe) # remove rows where probe is found as prb_1
        if (idx_data1.size is not 0):  
            for i in range(len(idx_data1.tolist())):    
                idx_to_remove.append(idx_data1.tolist()[i])
        
        idx_data2=numpy.argwhere(disease_array['prb_2']==probe)   
        if (idx_data2.size is not 0):  
            for i in range(len(idx_data2.tolist())):    
                idx_to_remove.append(idx_data2.tolist()[i])
        
    print('Number of rows in disease data containing probes to remove is %d'  % len(idx_to_remove))

    updated_array=numpy.delete(disease_data,idx_to_remove,axis=0)
    return updated_array

if __name__ == '__main__':

    data = read_table_file('../test/snp_sig_sel2w.txt')
    print(data.dtype)
    print(data[['study','rs_1','rs_2','fltChi2']])
