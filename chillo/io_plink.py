"""Interface to read PLINK format files"""

import os
from numpy import genfromtxt
from numpy import array, flatnonzero
from numpy import uint8

class GenotypeData(object):
    """
    Class for reading data in PLINK bed, bim, fam files
    """
    def __init__(self, filename, verbose=False):
        """Just check that the files exists"""
        self.verbose = verbose
        self.file_name = filename
        self.data_name = os.path.basename(filename)

        # probes and individuals are similar to PLINK BIM and FAM files resp.
        self.probes = None
        self.individuals = None

        # phenotype and genotype points to a HDF5 carray
        self.genotype = None
        self.phenotype = None

        if not os.path.isfile('%s.bed' % filename):
            print('%s.bed not found' % filename)
            return
        if not os.path.isfile('%s.bim' % filename):
            print('%s.bim not found' % filename)
            return
        if not os.path.isfile('%s.fam' % filename):
            print('%s.fam not found' % filename)
            return
        self.bed_name = '%s.bed' % filename
        self.bim_name = '%s.bim' % filename
        self.fam_name = '%s.fam' % filename

    @property
    def num_probes(self):
        """The number of loci tested, the number of genotypes per invididual"""
        return len(self.probes)

    @property
    def num_individuals(self):
        """The number of examples, the number of individuals in study"""
        return len(self.individuals)

    def open_file(self):
        """Load the BIM and FAM files"""
        self.probes = self.load_probes()
        self.individuals = self.load_individuals()
        self.init_phenotypes()

    def close_file(self):
        """Does nothing"""
        pass
        
    def init_phenotypes(self):
        """Extract the phenotype from the individuals"""
        if self.verbose: print('creating phenotype vector')
        phenotypes = self.individuals['phenotype']
        phenotypes[phenotypes==1] = -1
        phenotypes[phenotypes==2] = 1
        phenotypes.shape = (len(phenotypes),1)
        #check for undefined phenotype
        undefined_phenotype = flatnonzero(phenotypes==0)
        if len(undefined_phenotype) > 0:
            print('Some phenotypes were undefined')
            print(undefined_phenotype)
        self.phenotype = phenotypes

    def get_probe(self, idx_snp):
        """Return the probe information for snp idx_snp"""
        return self.probes[idx_snp]
        
    def get_genotype(self, idx_snp):
        """Return the array of genotypes for snp idx_snp"""
        from plinkio import plinkfile
        bed_file = plinkfile.open(self.file_name)
        for counter, row in enumerate(bed_file):
            if counter == idx_snp:
                data = array(list(row),dtype=uint8)
                break
        bed_file.close()
        return data

    def get_idx_case(self):
        """Return the index of individuals who are cases"""
        return flatnonzero(array(self.phenotype) == 1)

    def get_idx_control(self):
        """Return the index of individuals who are controls"""
        return flatnonzero(array(self.phenotype) == -1)

    def load_genotypes(self):
        """
        Load the plink BED format genotype data file.
        Assumes samples in columns and SNP loci in rows.

        Needs plinkio.
        https://bitbucket.org/mattias_franberg/libplinkio
        """
        from plinkio import plinkfile
        if self.verbose: print('Reading genotypes from %s' % self.file_name)
        bed_file = plinkfile.open(self.file_name)
        for counter, row in enumerate(bed_file):
            self.genotype[counter,:] = list(row)
            if counter % 100000 == 99999:
                print(counter+1)
        bed_file.close()
        
    def load_probes(self):
        """Read the BIM file to get the probe locations
        chromosome (1-22, X, Y or 0 if unplaced)
        rs# or snp identifier
        Genetic distance (morgans)
        Base-pair position (bp units)
        """
        if self.verbose: print('Reading probes from %s' % self.bim_name)
        probes = genfromtxt(open(self.bim_name, 'r'), delimiter='\t',
        dtype={'names': ['chrom','ID','distance','bp_position','allele1','allele2'],
               'formats':[int, 'S16', int, int, 'S1', 'S1']})
        return probes

    def load_individuals(self):
        """Read the FAM file to get information about individuals
        Family ID
        Individual ID
        Paternal ID
        Maternal ID
        Sex (1=male; 2=female; other=unknown)
        Phenotype
        """
        if self.verbose: print('Reading individuals from %s' % self.fam_name)
        individuals = genfromtxt(open(self.fam_name, 'r'),
        dtype={'names':['family','individual','paternal','maternal','sex','phenotype'],
               'formats':['S10','S16',int,int,int,int]})
        return individuals

