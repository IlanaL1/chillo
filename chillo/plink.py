"""Create a pythonic interface to command line PLINK"""

import os
import shutil
from subprocess import check_call
from numpy import genfromtxt, flatnonzero
from numpy import arange, argsort, log10

def call(data_name, input_dir=None, output_dir=None, hypo_test='assoc',
         cluster=False, verbose=False):
    """
    Run PLINK to discover univariate association.
    
    Construct the command line string and use
    subprocess.check_call to execute it
    """
    command_str = 'plink --noweb --%s ' % hypo_test
    if not verbose:
        command_str += '--silent '
    if input_dir is not None:
        command_str += '--bfile %s/%s ' % (input_dir, data_name)
    else:
        command_str += '--bfile %s ' % data_name
    if output_dir is not None:
        command_str += '--out %s/%s ' % (output_dir, data_name)
        if hypo_test == 'assoc':
            res_filename = '%s/%s.assoc' % (output_dir, data_name)
        else:
            res_filename = '%s/%s.assoc.%s' % (output_dir, data_name, hypo_test)
    else:
        command_str += '--out %s ' % data_name
        if hypo_test == 'assoc':
            res_filename = '%s.assoc' % data_name
        else:
            res_filename = '%s.assoc.%s' % (data_name, hypo_test)
        
    check_call(command_str, shell=True)

    return res_filename

def get_probes(res_filename, hypo_test, verbose=False, sig_level=None, num_probes=None):
    """Parse the results of PLINK
    and return the probes that are above Bonferroni correction.
    If num_probes is defined, then it overrides sig_level.
    """
    if hypo_test == 'assoc':
        data = genfromtxt(res_filename, skip_header=1,
                          missing_values=['NA'], filling_values=[1.],
        dtype={'names': ['CHR', 'SNP', 'BP', 'A1', 'F_A', 'F_U', 'A2', 'CHISQ', 'P', 'OR'],
               'formats': [int, 'S16', int, 'S1', float, float, 'S1', float, float, float]}
               )
    else:
        data = genfromtxt(res_filename, skip_header=1,
                          missing_values=['NA'], filling_values=[1.],
        dtype={'names': ['CHR', 'SNP', 'BP', 'A1', 'F_A', 'F_U', 'A2', 'P', 'OR'],
               'formats': [int, 'S16', int, 'S1', float, float, 'S1', float, float]}
               )
    if num_probes is not None:
        idx_sig = arange(len(data))
        p_values = -log10(data['P'])
        sort_idx = argsort(-p_values)
        sort_idx = sort_idx[:num_probes]
        idx_sig = idx_sig[sort_idx]
        p_values = p_values[sort_idx]
    else:
        num_probes = len(data)
        if sig_level is not None:
            idx_sig = flatnonzero(data['P'] < sig_level/num_probes)
            p_values = -log10(data[idx_sig]['P'])
        else:
            idx_sig = arange(num_probes)
            p_values = -log10(data['P'])
        sort_idx = argsort(-p_values)
        idx_sig = idx_sig[sort_idx]
        p_values = p_values[sort_idx]
    return idx_sig, p_values


def exclude_probe(data_name, probe_names, input_dir=None, verbose=False):
    """Write the list probe_names to file,
    then call plink to exclude them from the dataset.

    Construct the command line string and use
    subprocess.check_call to execute it
    """
    command_str = 'plink --noweb --make-bed '
    if not verbose:
        command_str += '--silent '
    if input_dir is not None:
        data_file = '%s/%s' % (input_dir, data_name)
    else:
        data_file = '%s' % data_name
    #for ext in ['bed','bim','fam']:
    #    shutil.copy2('%s.%s' % (data_file, ext), '%sp.%s' % (data_file, ext))
    command_str += '--bfile %s ' % data_file
    command_str += '--exclude temp.txt '
    command_str += '--out %sp ' % data_file

    to_exclude = open('temp.txt', 'w')
    for probe in probe_names:
        to_exclude.write('%s\n' % probe)
    to_exclude.close()

    check_call(command_str, shell=True)

    
