"""
Code to build a graph from pairs, and extract the connected subgraph.
Uses sparse matrices for memory efficiency.

This module contains code for building and manipulating graphs.

For more functionality, look a gwis_graph_dense.py
"""

from scipy.sparse.lil import lil_matrix
from scipy.sparse import triu
#from scipy.sparse.csc import csc_matrix as spmat
#from scipy.sparse.csgraph import laplacian, breadth_first_order
from scipy.io import mmio
import numpy
from numpy import array, argsort
from numpy import max, any, arange
from numpy import zeros, sqrt
from numpy import flatnonzero 
from numpy import diag, sort, unique
from numpy.random import rand
from collections import deque
from chillo.link_clustering import HLC
from chillo.tools_clustering import read_edgelist_unweighted_my, edgeTOcomm

have_networkx = True
try:
    import networkx as nx
except ImportError:
    have_networkx = False

class GWISGraph(object):
    """An annotated graph, where vertices and edges may both have names.
    Designed to visualize the snp pairs coming from GWIS
    """
    def __init__(self, name='GWISGraph', vertex_name='chroff'):
        self.name = name
        self.vertex_name = vertex_name
        self.num_vertex = 0
        self.vertex_names = array([])
        self.edge_names = array([])
        self.adj_mat = lil_matrix(0)
        self.diagonal = array([])
        self.diagonal_unweighted = array([])
        self.laplacian = lil_matrix(0)
        self.subgraph_id = array([])

        # Strings containing number of edges in subgraphs and communities
        self.subgraphs = ""
        self.communities = ""
        
        self.community_id = lil_matrix(0)
        if have_networkx:
            self.graph = nx.Graph()

    @property
    def num_connected_subgraph(self):
        return max(self.subgraph_id)

    def init_graph(self, adj_mat):
        """Copy the adjacency matrix, and initialize num_vertex and subgraph_id"""
        self.adj_mat = lil_matrix(adj_mat)
        self.num_vertex = self.adj_mat.shape[0]
        self.subgraph_id = zeros(self.num_vertex, dtype=int)
        self.diagonal = array(self.adj_mat.sum(0)).flatten()
        self.diagonal_unweighted = zeros(self.num_vertex)
        temp = self.adj_mat.tocsr()
        for idx in range(self.num_vertex):
            self.diagonal_unweighted[idx] = temp[idx,:].nnz
        self.community_id = lil_matrix((self.num_vertex,self.num_vertex)) #IL add to avoid errors
        
    def init_laplacian(self, normalized=True):
        """Construct the graph Laplacian from the adjacency matrix"""
        cur_diag = lil_matrix(self.adj_mat.shape)
        cur_diag.setdiag(self.diagonal)
        if normalized:
            sqrtinvdiag = lil_matrix(self.adj_mat.shape)
            sqrtinvdiag.setdiag(1.0/sqrt(self.diagonal))
            self.laplacian = sqrtinvdiag*(cur_diag - self.adj_mat)*sqrtinvdiag
        else:
            self.laplacian = cur_diag - self.adj_mat
        
    def graph_statistics(self):
        """Some simple statistics about the graph"""
        print('Number of vertices = %d' % self.num_vertex)

    def get_neighbours(self, vertex):
        """Return the neighbours of vertex"""
        return self.adj_mat[:,vertex].nonzero()[0]

    def bfs_fill(self, init_vertex, colour):
        """Starting from init_vertex, fill in all the neighbours with colour."""
        Q = deque([init_vertex])
        seen = [init_vertex]
        while len(Q) > 0:
            cur_vertex = Q.popleft()
            neighbours = self.get_neighbours(cur_vertex)
            for neigh in neighbours:
                if neigh not in seen:
                    seen.append(neigh)
                    Q.append(neigh)
        for vertex in seen:
            if self.subgraph_id[vertex] > 0 and self.subgraph_id[vertex] != colour:
                print('Error: vertex  %d already coloured by other colour %d'\
                    % (vertex, self.subgraph_id[vertex]))
            else:
                self.subgraph_id[vertex] = colour
        
    def bfs_fill_recursive(self, init_vertex, colour):
        """Starting from init_vertex, fill in all the neighbours with colour.
        Using recursion
        """
        if self.subgraph_id[init_vertex] > 0 and self.subgraph_id[init_vertex] != colour:
            print('Error: vertex already coloured by other colour')
        elif self.subgraph_id[init_vertex] == colour:
            return
        self.subgraph_id[init_vertex] = colour
        neighbours = self.get_neighbours(init_vertex)
        for neigh in neighbours:
            self.bfs_fill_recursive(neigh, colour)

    def bfs_fill_sparse(self, init_vertex, colour):
        """Fill all nodes connected to init_vertex with colour"""
        fill_idx = breadth_first_order(self.adj_mat, init_vertex, directed=False)
        if any(self.subgraph_id[fill_idx] > 0):
            print('Error: vertex already coloured by other colour')
        self.subgraph_id[fill_idx] = colour

    def colour_connected(self):
        """Colour each connected component of a graph with a new colour"""
        init_vertex = 0
        colour = 1
        self.adj_mat = self.adj_mat.tocsc()
        while True:
            self.bfs_fill(init_vertex, colour)
            not_labeled = flatnonzero(self.subgraph_id == 0)
            if len(not_labeled) == 0:
                break
            init_vertex = min(not_labeled)
            colour += 1
        self.adj_mat = self.adj_mat.tolil()
        print('Found %d connected components' % colour)

    def count_vertex_colour(self):
        """Return the number of vertices for each subgraph"""
        assert(len(flatnonzero(self.subgraph_id == 0))==0)
        num_colour = int(max(self.subgraph_id))
        vertex_count = zeros(num_colour, dtype=int)
        for colour in arange(1,num_colour+1):
            vertex_count[colour-1] = len(flatnonzero(self.subgraph_id == colour))
        return num_colour, vertex_count

    def sort_colours(self):
        """Renumber the subgraphs such that the smallest id contains the most nodes"""
        num_colour, vertex_count = self.count_vertex_colour()
        sort_idx = argsort(-vertex_count)
        colours = zeros(num_colour, dtype=int)
        colours[sort_idx] = arange(num_colour)+1
        for idx in range(self.num_vertex):
            self.subgraph_id[idx] = colours[self.subgraph_id[idx]-1]
            
    def find_large_components(self, min_vertex_count=5):
        """Return the colours of the large components"""
        num_colour, vertex_count = self.count_vertex_colour()
        idx = flatnonzero(vertex_count>min_vertex_count)
        return (idx+1, list(map(int,vertex_count[idx])))
        
    def extract_subgraph(self, colour):
        """Construct a subgraph with the given colour,
        and return it
        """
        if (self.num_connected_subgraph==1):
            return self
        else:
            subgraph = GWISGraph('subgraph_%02d' % colour)
            vertices = flatnonzero(self.subgraph_id == colour)
            subgraph.num_vertex = len(vertices)
            subgraph.vertex_names = self.vertex_names[vertices]
            subgraph.edge_names = self.edge_names[vertices,:][:,vertices]
            subgraph.adj_mat = self.adj_mat[vertices,:][:,vertices]
            subgraph.diagonal = self.diagonal[vertices]
            subgraph.diagonal_unweighted = self.diagonal_unweighted[vertices]
            subgraph.subgraph_id = self.subgraph_id[vertices]
            subgraph.community_id = self.community_id[vertices,:][:,vertices]
            return subgraph



    def find_hub_ids(self, min_hub_size=3):
        """Return the colours of the large components"""
        hub_id_list=[]
        for idx in range(self.num_vertex):
            if self.diagonal_unweighted[idx]>min_hub_size:
                hub_id_list.append(idx)
        return hub_id_list
        
    def extract_snp_subgraph(self, snp_id):
        """Construct a subgraph from the snp_id, by extracting the neighbours
        snp_id is usually hub_id from function
        """
        subgraph = GWISGraph('snp_subgraph_%02d' % snp_id)
        vertices=flatnonzero(self.adj_mat.toarray()[snp_id,:])
        vertices=vertices.tolist()
        vertices.append(snp_id) # append snp_id 
        # vertices are all nodes connected to hub
        subgraph.num_vertex = len(vertices)
        subgraph.vertex_names = self.vertex_names[vertices]
        subgraph.edge_names = self.edge_names[vertices,:][:,vertices]
        subgraph.adj_mat = self.adj_mat[vertices,:][:,vertices]
        subgraph.diagonal = self.diagonal[vertices]
        subgraph.diagonal_unweighted = self.diagonal_unweighted[vertices]
        subgraph.subgraph_id = self.subgraph_id[vertices]
        subgraph.community_id = self.community_id[vertices,:][:,vertices]
        return subgraph

    def extract_bipartite_subgraph(self, clus_1, clus_2,vertices):
        """Construct a subgraph between snps(vertices) in clus_1 and clus_2,
        and return it. Pass in vertices belonging to clusters. (clus_1, and 2 used for naming only)
        Currently returns all edges (ie including between nodes in clus 1 and clus1)
        """
        subgraph = GWISGraph('bipartite_subgraph_%02d_%02d' % (clus_1,clus_2))
        #vertices=flatnonzero(data.bedline['clus_id']==clus_1).tolist()+flatnonzero(data.bedline['clus_id']==clus_2).tolist()
        # vertices are all nodes connected to hub
        subgraph.num_vertex = len(vertices)
        subgraph.vertex_names = self.vertex_names[vertices]
        subgraph.edge_names = self.edge_names[vertices,:][:,vertices]
        subgraph.adj_mat = self.adj_mat[vertices,:][:,vertices]
        subgraph.diagonal = self.diagonal[vertices]
        subgraph.diagonal_unweighted = self.diagonal_unweighted[vertices]
        subgraph.subgraph_id = self.subgraph_id[vertices]
        subgraph.community_id = self.community_id[vertices,:][:,vertices]
        return subgraph

    def init_networkx(self):
        """Construct the networkx Graph"""
        for idx, vert in enumerate(self.vertex_names):
            self.graph.add_node(idx, label=vert, degree=self.diagonal[idx])
        for idx1 in range(self.num_vertex):
            for idx2 in range(idx1, self.num_vertex):
                if self.adj_mat[idx1,idx2] > 0.0:
                    self.graph.add_edge(idx1, idx2, weight=self.adj_mat[idx1, idx2])

    def export2mtx(self, filename):
        """Write the adjacency matrix to filename"""
        print('Exporting to %s' % filename)
        mmio.mmwrite(filename, triu(self.adj_mat), comment=self.name)

    def find_community(self):
        """Use information in self.adj_mat
        Write self.community_id i.e. the identity of the different 'clusters'
        in a particular connected subgraph. The clusters are identified
        using link clustering.
        """
        find="no"
        
        self.community_id = lil_matrix(self.adj_mat.shape)

        adj_mat_tmp = lil_matrix(self.adj_mat.copy())        

        num_rows, num_cols = self.adj_mat.shape
        for i in range(self.num_connected_subgraph):

            vertices_comm = flatnonzero(self.subgraph_id == i+1)

            #print vertices_comm

            #mat = lil_matrix(self.extract_subgraph(i+1).adj_mat)


            adj, edges = read_edgelist_unweighted_my(self.extract_subgraph(i+1).adj_mat)

            edge2cid, S_max, D_max, list_D = HLC( adj,edges ).single_linkage()


            for list_e2c in edgeTOcomm(edge2cid): 


                self.community_id[vertices_comm[list_e2c[0]],vertices_comm[list_e2c[1]]] = str(i+1) +"."+ str(list_e2c[2]) #i+1+(float(list_e2c[2])/10)
                self.community_id[vertices_comm[list_e2c[1]],vertices_comm[list_e2c[0]]] = str(i+1) +"."+ str(list_e2c[2])


           
#                for l in  range(num_rows):
#                        if mat[list_e2c[0],list_e2c[1]] in self.adj_mat.getrow(l).toarray(): 
#                        if mat[list_e2c[0],list_e2c[1]] in adj_mat_tmp.getrow(l).toarray(): 
#                               # print mat[list_e2c[0],list_e2c[1]], adj_mat_tmp.getrow(l).toarray(),"ok",l
#                                for c in  range(num_cols):
#                                      if mat[list_e2c[0],list_e2c[1]]==self.adj_mat[l,c]: 
#                                      if mat[list_e2c[0],list_e2c[1]]==adj_mat_tmp[l,c]:                  
                                                
#                                            self.community_id[l,c] = str(i+1) +"."+ str(list_e2c[2]) #i+1+(float(list_e2c[2])/10)
#                                            self.community_id[c,l] = str(i+1) +"."+ str(list_e2c[2])
#                                            adj_mat_tmp[l,c]=-999999  #avoid old values
                                        


 
    def count_edges_subgraphs(self):
        """Count the number of edges in each connected subgraph"""
        self.num_edges_subgraphs = zeros(self.num_connected_subgraph, dtype=int)
        for ix in range(self.num_connected_subgraph):
            cur_graph = self.extract_subgraph(ix+1)
            self.num_edges_subgraphs[ix] = cur_graph.adj_mat.nnz/2

        self.subgraphs = '"subgraphs": ['
        for ix in range(self.num_connected_subgraph):
            self.subgraphs += '{"probe_group": ' + str(ix+1)
            self.subgraphs += ', "edge_count": '+ str(self.num_edges_subgraphs[ix]) +'}, '
        self.subgraphs=self.subgraphs[:-2]+']'
            
            

    def count_edges_subgraphs_old(self): 
        """Count the number of edges in each connected subgraph,
        and each community"""
#        self.subgraphs = '"subgraphs": [{"subgraph_id": 1, "edge_count": 5}, {"subgraph_id": 2, "edge_count": 7}] '
#        self.communities = '"communities": [{"comm_id": 1.1, "edge_count": 3}, {"comm_id":1.2, "edge_count":2}]'
        n_max=self.num_connected_subgraph
        self.subgraphs = '"subgraphs": ['
        count_es=0
        count_ec=0
        list_comm_id=[]        #    imp
        num_rows, num_cols = self.community_id.shape
        for i in range(n_max):
            for l in  range(num_rows): 
                for c in  range(num_cols):
                    if (l<c and self.community_id[l,c]>0
                        and self.community_id[l,c]>(i+1)
                        and self.community_id[l,c]<(i+2)):
                        count_es=count_es+1
                        list_comm_id.append(float(self.community_id[l,c]))
            self.subgraphs =self.subgraphs+'{"probe_group": '+str(i+1)+', "edge_count": '+ str(count_es) +'}, ' # replace "subgraph_id" for "probe_group"
            count_es=0

        self.subgraphs=self.subgraphs[:-2]+']'





    def count_edges_communities(self): 
        """Count the number of edges in each connected subgraph,
        and each community"""
#        self.subgraphs = '"subgraphs": [{"subgraph_id": 1, "edge_count": 5}, {"subgraph_id": 2, "edge_count": 7}] '
#        self.communities = '"communities": [{"comm_id": 1.1, "edge_count": 3}, {"comm_id":1.2, "edge_count":2}]'
        n_max=self.num_connected_subgraph
       
        
        count_ec=0
        list_comm_id=[]        #    imp
        num_rows, num_cols = self.community_id.shape
        for i in range(n_max):
            for l in  range(num_rows): 
                for c in  range(num_cols):
                    if (l<c and self.community_id[l,c]>0
                        and self.community_id[l,c]>(i+1)
                        and self.community_id[l,c]<(i+2)):
                       
                        list_comm_id.append(float(self.community_id[l,c]))
            
        self.communities = '"communities": ['
        min_v = min(list_comm_id)
        max_v = max(list_comm_id)
        while min_v < max_v  :
            count_ec=list_comm_id.count(min_v)
            self.communities =self.communities+'{"assoc_group": '+str(min_v)+', "edge_count": '+ str(count_ec) +'}, ' # replace "comm_id" for "assoc_group"
            for i in range(count_ec):
                list_comm_id.remove(min_v)
            min_v = min(list_comm_id)  
        self.communities =self.communities+'{"assoc_group": '+str(max_v)+', "edge_count": '+ str(list_comm_id.count(max_v)) +'}]' # replace "comm_id" for "assoc_group"







    
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True, exclude_empty=True)
    
