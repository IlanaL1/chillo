"""Some tools for functions commonly needed for numpy arrays"""

from numpy import empty, unique, flatnonzero

def find(ar1, ar2):
    """Given the two input arrays ar1, ar2,
    return idx1, idx2 which are arrays of the same length respectively.
    Assume that ar1 and ar2 are unique.
    idx1 contains the indices of the element in ar2 that match ar1,
    and vice versa.
    
    >>> ar1 = array([2,4,6])
    >>> ar2 = array([1,2,3,4])
    >>> idx1, idx2 = find(ar1, ar2)
    >>> idx1 == array([1,3,nan])
    >>> idx2 == array([nan, 0, nan, 1])
    """
    idx1 = empty(len(ar1))*numpy.nan
    idx2 = empty(len(ar2))*numpy.nan
    for ix,elem in enumerate(ar1):
        ix2 = flatnonzero(elem==ar2)
        if len(ix2) > 0:
            idx1[ix] = ix2
            idx2[ix2] = ix
    return idx1, idx2

def unique_rows(a):
    """Return unique rows of a. Assumes 2 columns"""
    unique_a = unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))

