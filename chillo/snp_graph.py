"""Container for a graph arising from GWIS SNP pairs.

Implements functionality where the vertices of a graph are SNP probes.

"""

import sys, os
import itertools
import csv
from scipy.io import loadmat
import scipy.sparse
from scipy.sparse.lil import lil_matrix
from scipy.stats import chi2
import numpy
from numpy import array, matrix, allclose,triu
from numpy import max, sum, log10
from numpy import hstack, vstack, zeros, ones, empty
from numpy import nonzero, flatnonzero
from numpy import diag, argsort, sort, unique
from numpy.linalg import eigh
from numpy.random import rand
from chillo.graph import GWISGraph
from chillo.io_plink import GenotypeData as GenotypeDataIOPlink
#from chillo.io_hdf5 import GenotypeData
from chillo.io_sql import GenotypeData
from chillo.ld_functions import calc_LD
from chillo.roc_plot import  roc_plot
from chillo.contin_table import contingency_table


from chillo.tools_overlap import find_gene_intersection




have_plot = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    have_plot = False

have_networkx = True
try:
    from networkx_tools import view_graph_spring
except ImportError:
    have_networkx = False

have_pytables = True
try:
    import tables
except ImportError:
    have_pytables = False



class SnpGraph(GWISGraph):
    """A container for representing the graph of SNP pairs."""
    def __init__(self, name, vertex_name='chroff', bonferroni=0.0):
        super(SnpGraph, self).__init__(name, vertex_name)
        self.snp = array([]) # store chrom, bp_position and rs from .bim file
        self.gene_name=array([]) # store a gene name
        self.gene_list=array([]) # should merge gene_list with gene_name ultimately (gene_list is better structure)
        self.chroff = array([])
        self.rs = array([])
        self.bedline = array([])
        self.idx2snp = array([])
        self.snp2idx = array([])
        self.bonferroni = bonferroni
        self.assoc = dict()

    def __repr__(self):
        """The name"""
        return '<SnpGraph(%s)>' % self.name
    
    @property
    def num_snps(self):
        """The number of vertices"""
        return self.num_vertex

    def init_from_sarray(self, input_data, snp_data, stat_tests):
        """Initialize class from numpy structured array provided by input_data.
        stat_tests lists the names of the columns that we parse for each pair.
        For snps, we expect columns (two of them):
        - rs
        - prb
        - browser
        - prbCode
        """
        #for test in stat_tests:
            #self.set_bonferroni_test(test,input_data.shape)
        # problem, setting bonferroni determines which pairs you read in.
        # currently no structure in snp_graph to store different lists of snps

        self._init_snp2idx(input_data['prb_1'], input_data['prb_2'])
        self.snp = zeros(self.num_vertex, #initialize with zeros
            dtype={'names':['chrom','rs','bp_position','prbCode','prb'],
                   'formats':[int,'S16',int,'S16',int]})

        all_probes=unique(input_data['prb_1'].tolist()+input_data['prb_2'].tolist())

        for idx,elem in enumerate(all_probes):
            snp_idx = self.snp2idx[elem]
            #snp_details=snp_data[snp_data['prb']==elem,:]
            snp_details=snp_data[snp_data['prb']==elem,] ##removed slicing for numpy 1.8

            if snp_details.size==0:
                print('error: snp %d not found in snp array' % snp_idx)
                return
            else:
                snp_details=snp_details[0]
                self.snp['rs'][snp_idx] = snp_details['rs']
                self.snp['chrom'][snp_idx] = snp_details['chrom']
                self.snp['bp_position'][snp_idx] = snp_details['bp_position']
                self.snp['prbCode'][snp_idx] = snp_details['prbCode']
                self.snp['prb'][snp_idx] = elem

        # Create members for code to work. To remove later.
        self.rs = self.snp['rs']
        self._init_chroff()
        self._init_bedline()
        
        #for assoc_name in ['fltGSS_prtv', 'fltGSS', 'fltGSS_cntr', 'fltSS', 'fltDSS', 'fltChi2']:
        for assoc_name in stat_tests:
            self._init_graph_sparse(assoc_name, input_data['prb_1'], input_data['prb_2'], input_data[assoc_name])

        # Create members for code to work. To remove later.
        self.set_adj_mat(input_data.dtype.names[2])
        self.init_graph_labels()


    def init_from_sarray_plink(self, input_data, stat_tests):
        """Initialize class from numpy structured array provided by input_data.
        stat_tests lists the names of the columns that we parse for each pair.
        For snps, we expect columns (two of them):
        - rs
        - prb
        - browser
        - prbCode
        """
        self._init_snp2idx(input_data['prb_1'], input_data['prb_2'])
        self.snp = zeros(self.num_vertex, #initialaze with zeros
            dtype={'names':['chrom','rs','bp_position','prbCode','prb'],
                   'formats':[int,'S16',int,'S16','S16']})

        for idx,elem in enumerate(input_data['prb_1']):
            snp_idx = self.snp2idx[elem]

            self.snp['rs'][snp_idx] = str(input_data['SNP1'][idx])
            self.snp['chrom'][snp_idx] = int(input_data['CHR1'][idx])
            self.snp['bp_position'][snp_idx] = int(input_data['position1'][idx])
            self.snp['prbCode'][snp_idx] = str(input_data['SNP1'][idx])
            self.snp['prb'][snp_idx] = str(input_data['prb_1'][idx])

 
        for idx,elem in enumerate(input_data['prb_2']):
            snp_idx = self.snp2idx[elem]

            self.snp['rs'][snp_idx] = str(input_data['SNP2'][idx])
            self.snp['chrom'][snp_idx] = int(input_data['CHR2'][idx])
            self.snp['bp_position'][snp_idx] = int(input_data['position2'][idx])
            self.snp['prbCode'][snp_idx] = str(input_data['SNP2'][idx])
            self.snp['prb'][snp_idx] = str(input_data['prb_2'][idx])

        # Create members for code to work. To remove later.
        self.rs = self.snp['rs']
        self._init_chroff()
        self._init_bedline()
        
        #for assoc_name in ['fltGSS_prtv', 'fltGSS', 'fltGSS_cntr', 'fltSS', 'fltDSS', 'fltChi2']:
        for assoc_name in stat_tests:
            self._init_graph_sparse(assoc_name, input_data['prb_1'], input_data['prb_2'], input_data[assoc_name])

        # Create members for code to work. To remove later.
        self.set_adj_mat(stat_tests[0])
        self.init_graph_labels()


    def init_from_sarray_gwis_data(self, input_data, stat_tests):
        """Initialize class from numpy structured array provided by input_data.
        stat_tests lists the names of the columns that we parse for each pair.
        For snps, we expect columns (two of them):
        - rs
        - prb
        - browser  
        - prbCode
        """
        self._init_snp2idx(input_data['prb1'], input_data['prb2'])
        self.snp = zeros(self.num_vertex, #initialaze with zeros 
            dtype={'names':['chrom','rs','bp_position','prbCode','prb'],
                   'formats':[int,'S16',int,'S16','S16']})

        for idx,elem in enumerate(input_data['prb1']):
            snp_idx = self.snp2idx[elem]

            self.snp['rs'][snp_idx] = str(input_data['rs_1'][idx])
            self.snp['chrom'][snp_idx] = int(input_data['chr_1'][idx])
            self.snp['bp_position'][snp_idx] = int(input_data['bp_1'][idx])
            self.snp['prbCode'][snp_idx] = str(input_data['rs_1'][idx])
            self.snp['prb'][snp_idx] = str(input_data['prb1'][idx])

 
        for idx,elem in enumerate(input_data['prb2']):
            snp_idx = self.snp2idx[elem]

            self.snp['rs'][snp_idx] = str(input_data['rs_2'][idx])
            self.snp['chrom'][snp_idx] = int(input_data['chr_2'][idx])
            self.snp['bp_position'][snp_idx] = int(input_data['bp_2'][idx])
            self.snp['prbCode'][snp_idx] = str(input_data['rs_2'][idx])
            self.snp['prb'][snp_idx] = str(input_data['prb2'][idx])

        # Create members for code to work. To remove later.
        self.rs = self.snp['rs']
        self._init_chroff()
        self._init_bedline()
        
        #for assoc_name in ['fltGSS_prtv', 'fltGSS', 'fltGSS_cntr', 'fltSS', 'fltDSS', 'fltChi2']:
        for assoc_name in stat_tests:
            self._init_graph_sparse(assoc_name, input_data['prb1'], input_data['prb2'], input_data[assoc_name])

        # Create members for code to work. To remove later.
        self.set_adj_mat(stat_tests[0])
        self.init_graph_labels()



    def _init_graph_sparse(self, assoc_name, snp1, snp2, val):
        """Construct the weighted graph from the snp pairs.

        Graph features are:
        - adj_mat: adjacency matrix
        - diagonal: column sum of adjacency matrix

        TODO: Modify to allow for missing values (NaN)
        """
        adj_mat = lil_matrix((self.num_vertex,self.num_vertex))
        for elem in zip(snp1,snp2,val):
            if elem[2] < 1e-5: continue
            s1 = self.snp2idx[elem[0]]
            s2 = self.snp2idx[elem[1]]
            v = elem[2]
            if adj_mat[s1, s2] > 0.0:
                print('_init_graph_sparse: error (already seen) %d, %d = %f' % (elem[0],elem[1],v))
                continue
            adj_mat[s1, s2] = v 
            adj_mat[s2, s1] = v

        self.assoc[assoc_name] = adj_mat



    def set_adj_mat(self, assoc_name):
        """Set the variable adj_mat to point to the value assoc_name in dictionary self.assoc.
        self.adj_mat is used by super class GWIS graph to represent the adjacency matrix.
        """
        self.init_graph(self.assoc[assoc_name])


    def get_contingency_tables(self, geno_file):
        """Compute the contingency tables"""
        if os.path.exists('%s.db' % geno_file):
            geno_data = GenotypeData(geno_file)
        else:
            geno_data = GenotypeDataIOPlink(geno_file)
        geno_data.open_file()
        idx_cases = geno_data.get_idx_case()
        idx_controls = geno_data.get_idx_control()
        self.contingency_tables = {}
        edges = scipy.sparse.triu(self.adj_mat.tocoo())
        for row, col, v in itertools.izip(edges.row, edges.col, edges.data):
            assert(v > 0)
            dataset = contingency_table(self.idx2snp[row]-1, self.idx2snp[col]-1, 
                                        idx_controls, idx_cases, geno_data)
            self.contingency_tables[row, col] = dataset
        geno_data.close_file()
        
    def _get_json_node(self, idx):
        """Return a JSON formatted node corresponding to the row idx in self.snp"""

        str_json= '{"prbCode": "'+self.snp['prbCode'][idx]
        str_json=str_json+'", "degree": '+str(self.diagonal_unweighted[idx]) 
        str_json=str_json+', "prb": '+str(self.snp['prb'][idx])
        str_json=str_json+', "rs": "'+str(self.snp['rs'][idx])
        str_json=str_json+'", "probe_group": '+str(self.subgraph_id[idx])
        str_json=str_json+', "bp_position": '+str(self.snp['bp_position'][idx])
        str_json=str_json+', "chrom": '+str(self.snp['chrom'][idx])
        str_json=str_json+', "id": '+str(idx)+'}'
     
        return str_json

    def _get_json_link(self, idx1, idx2):
        """Return a JSON formatted link corresponding to row (idx1) and column (idx2) in self.assoc"""
        
        str_json=""

        if self.community_id[idx1,idx2]==0 :

            str_json='{'

        else:
            
            str_json='{"assoc_group": '+str(self.community_id[idx1,idx2])+', ' # replace "comm_id" for "assoc_group"

        str_json=str_json+'"source": '+str(idx1)
        str_json=str_json+', "target": '+str(idx2)
        str_json=str_json+', "probe_group": '+str(self.subgraph_id[idx1]) # replace "subgraph_id" for "probe_group" 
   
        for name in self.assoc.keys():        
            str_json=str_json+', "'+name+'": '+str(self.assoc[name][idx1,idx2])       
        str_json=str_json+'}' 

        return str_json

    def _get_json_roc(self, idx1, idx2,filename):
        """return a object with dots for gen1 (9 dots), gen2 (9 dots)
           and gen1gen2 (512 dots) e.g {"gen1":[{}] ,"gen2":[{}] ,"gen1gen2":[{}] }  """

        #"roc":[{"gen1":[] ,"gen2":[] ,"gen1gen2":[] }, {"gen1":[] ,"gen2":[] ,"gen1gen2":[] }]        
        #{"gen1":[] ,"gen2":[] ,"gen1gen2":[] }           

        dataset=roc_plot(self.idx2snp[idx1]-1, self.idx2snp[idx2]-1, filename) #IMP

        #str_json='{"gen1":'+ str(dataset[0])
        str_json='{"gen1":['
    
        for i in dataset[0]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '
        
        str_json=str_json[:-2]+'] ,"gen2":['#+ str(dataset[1])

        for i in dataset[1]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '

        str_json=str_json[:-2]+'] ,"gen1gen2":['#+ str(dataset[2])+' }'
        

        for i in dataset[2]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '

        return str_json[:-2]+']}'

    def _get_json_ct(self, idx1, idx2):
        """return a object with values for contigency table unv1 (6 values), unv2 (6 values)
           and biv (18 values)"""

        dataset = self.contingency_tables[idx1, idx2]
        #dataset=contingency_table(self.idx2snp[idx1]-1, self.idx2snp[idx2]-1, filename)  #IMP 

#        str_json='{"unv1": [{"controls":'+str(dataset[0][0][0]/float(dataset[3][0]))+' ,"cases":'+str(dataset[0][1][0]/float(dataset[3][1]))+" }, "+'{"controls":'+str(dataset[0][0][1]/float(dataset[3][0]))+' ,"cases":'+str(dataset[0][1][1]/float(dataset[3][1]))+" }, "+'{"controls":'+str(dataset[0][0][2]/float(dataset[3][0]))+' ,"cases":'+str(dataset[0][1][2]/float(dataset[3][1]))+" }], " 


        str_json='{"unv1": [{"controls":'+str(dataset[0][0][0])+' ,"cases":'+str(dataset[0][1][0])+" }, "+'{"controls":'+str(dataset[0][0][1])+' ,"cases":'+str(dataset[0][1][1])+" }, "+'{"controls":'+str(dataset[0][0][2])+' ,"cases":'+str(dataset[0][1][2])+" }], " 


#print str_json,"\n"

#        str_json=str_json+'"unv2": [{"controls":'+str(dataset[1][0][0]/float(dataset[3][0]))+' ,"cases":'+str(dataset[1][1][0]/float(dataset[3][1]))+" }, "+'{"controls":'+str(dataset[1][0][1]/float(dataset[3][0]))+' ,"cases":'+str(dataset[1][1][1]/float(dataset[3][1]))+" }, "+'{"controls":'+str(dataset[1][0][2]/float(dataset[3][0]))+' ,"cases":'+str(dataset[1][1][2]/float(dataset[3][1]))+" }], "

        str_json=str_json+'"unv2": [{"controls":'+str(dataset[1][0][0])+' ,"cases":'+str(dataset[1][1][0])+" }, "+'{"controls":'+str(dataset[1][0][1])+' ,"cases":'+str(dataset[1][1][1])+" }, "+'{"controls":'+str(dataset[1][0][2])+' ,"cases":'+str(dataset[1][1][2])+" }], "


#print str_json,"\n"


        str_json=str_json+'"biv": ['

        for l in range(3):
            line='['
            for c in range(3):
#                line=line+'{"controls":'+str(dataset[2][0][l][c]/float(dataset[3][0]))+' ,"cases":'+str(dataset[2][1][l][c]/float(dataset[3][1]))+'}, '
                line=line+'{"controls":'+str(dataset[2][0][l][c])+' ,"cases":'+str(dataset[2][1][l][c])+'}, '


                       
            str_json=str_json+line[:-2]+'],'

        return str_json[:-1]+"]"+ ', "total": {"controls":'+str(dataset[3][0])+' ,"cases":'+  str(dataset[3][1]) +" }}"




  
    def export_json(self, filename_out, roc=True):
        """Export contents to JSON file suitable for visualization with D3.js"""

        # Nodes
        str_json='{"name": "'+self.name+'", "nodes": ['
        for i in range(self.num_vertex):
            str_json = str_json+ self._get_json_node(i)+', '

        # Links
        str_json=str_json[:-2]+'], "links": ['
        num_rows, num_cols = self.adj_mat.shape
        ct_id=0
        for l in  range(num_rows): 
            for c in  range(num_cols):
                if l<c and self.adj_mat[l,c]>0:
                    str_json += self._get_json_link(l,c)[:-1]
                    if roc:
                        str_json += ', "ct_id":'+str(ct_id)
                        ct_id=ct_id+1
                    str_json += '}, '
        str_json = str_json[:-2]+']'

        # Contingency table
        if roc:
            ct_id=0
            str_ct=' "cont_table":['

            for l in  range(num_rows): 
                for c in  range(num_cols):
                    if l<c and self.adj_mat[l,c]>0:
                        str_ct += self._get_json_ct(l, c)+', '
                        ct_id=ct_id+1
            str_ct = str_ct[:-2]+']'
                    
        # Subgraphs and communities
        str_json += ', ' + self.subgraphs
        if self.communities != "":
            str_json += ', ' + self.communities

        if roc:
            str_json +=  ", "+str_ct
        str_json += "}"

        f = open(filename_out, 'w')
        f.write(str_json)
        f.close()

    def num_snps_mapped(self):
        """ The number of snps with at least one gene mapped"""
        num_snps_mapped=0
        for idx in range(self.num_vertex):
            first_gene=self.gene_list['gene'][idx][0][0] # 1st[0]=row, 2nd[0]=col
            if first_gene: #(not empty)
                num_snps_mapped+=1
        return num_snps_mapped

    def set_bonferroni(self, stat, total_snps, bonferroni='None'): 
        """Set the threshold for Bonferroni correction,
        in terms of p-value.""" 
        if bonferroni is None:   # if not set
            #total_snps=    
            #total_snps = 449471 #IL
            ##total_snps = 536545  # bc571
            ##total_snps = 366 # small bc
            if stat is 'Chi2':            
                pval = 2.*0.05/(total_snps*(total_snps-1))
                self.bonferroni = chi2.ppf((1-pval),8) ## highest percentilem df=4 4, ppf is inverse of cdf
            elif stat is 'SS' or stat is 'DSS':
                print(stat)
                self.bonferroni = -log10(0.05) # assume importing -log(p value). p value is already bonferroni corrected
            elif stat is 'GSS':
                print(stat)
                self.bonferroni=0
            else:                
                self.bonferroni = -log10(2.*0.05/(total_snps*(total_snps-1))) # assume importing -log(p value). p value is not already bonferroni corrected
        else:
            self.bonferroni = bonferroni


        
    def remove_missing_snps(self,all_pairs=None, remove_snp_list=None):
        """Read GWIS output file format, and form the adjacency matrix
        remove any pairs that are in remove_snp_list
        assumes data.snp exists and is filled"""

        if remove_snp_list is not None:
            idx_list=[]
            for idx,snp in enumerate(all_pairs):
                rs1=self.snp['rs'][self.snp2idx[snp[0]]]
                rs2=self.snp['rs'][self.snp2idx[snp[1]]]
                if rs1 in remove_snp_list or rs2 in remove_snp_list:
                   idx_list.append(idx)
                   print('remove pair %s %s' % (rs1,rs2))
            all_pairs=numpy.delete(all_pairs,idx_list,axis=0)

            snp_idx_list=[]
            for idx,snp in enumerate(remove_snp_list):
                idx=nonzero(self.snp['rs']==remove_snp_list[idx])
                snp_idx_list.append(idx)



        #self._init_snp2idx(all_pairs['snp1'], all_pairs['snp2'])
        #data.snp=delete(data.snp,snp_idx_list) # not game to implement this yet, can replace repeat call to read_loci
        #self._init_graph_sparse(stat,all_pairs['snp1'], all_pairs['snp2'], all_pairs['score'])
        #self._init_chroff()
        #self._init_bedline()
        #self.init_graph_labels()
        return all_pairs

    def initialise_snps_and_sparse_graph(self,all_pairs,stat,BIM_file,rs_location_file=None,rs_file_cols=None):
        """ initialise snp2idx,idx2snp, data.snp, data.adj_mat etc """    
        self._init_snp2idx(all_pairs['snp1'], all_pairs['snp2'])
        pairs_not_found=self.read_loci(BIM_file,rs_location_file,rs_file_cols) # 
        self._init_graph_sparse(stat,all_pairs['snp1'], all_pairs['snp2'], all_pairs['score'])
        self.set_adj_mat(stat)
        self._init_chroff()
        self._init_bedline()
        self.init_graph_labels()
        return pairs_not_found

# remove_pairs(all_pairs, remove_snp_list) 
# all_pairs is output of read_gwis_file
# script calls read_gwis_file, read_loci, remove_pairs
# call init function
    def read_loci(self,BIM_file,rs_location_file=None,rs_file_cols=None):
        """ 
        Read locations of SNP ids from BIM file
        If rs_location_file and rs_file_cols are specied, read in this file as updated location
        rs_file_cols must specify columns in rs_location_file for chrom,bp_position and rs
        """
        all_snps = numpy.genfromtxt(open(BIM_file, 'rb'), usecols=(0,1,3),
                                dtype={'names': ['chrom','rs','bp_position'],
                                       'formats':['S16', 'S16', int]})
            #self.snp = all_snps[self.idx2snp] # commented and replaced with below because idx2snp references rows starting from 1. bim read in to starts from index 0. 
        self.snp = all_snps[self.idx2snp-1]
        self.rs = self.snp['rs']
        
        rs_not_found=[]  # list to store rs of snps that are not found
        if os.path.isfile(rs_location_file):  
            snp_location = numpy.genfromtxt(open(rs_location_file, 'rb'),                   
                           usecols=rs_file_cols, 
                           dtype={'names': ['chrom','bp_position','rs'], 'formats':['S16', int,'S16']}) 

            counter=0 # useful for debugging to count number of matches of rs numbers in in RS_LOCATION        
            for idx,rs in enumerate(self.rs):
                rs_row=snp_location[(snp_location['rs']==rs),:]
                if len(rs_row) >1:
                    print('RS location file contains more than one entry for %s' % rs) 
                if len(rs_row) is not 0:            
                    #self.snp[idx]['rs'] = rs_row['rs']
                    self.snp[idx]['chrom'] = rs_row['chrom'][0].split('chr')[1]
                    self.snp[idx]['bp_position'] = rs_row['bp_position']
                else: 
                    print(idx,rs)
                    rs_not_found.append(rs)
                    counter=counter+1

            print('Number of snps not found is %d'  % counter)

        #self._init_chroff()
        #self._init_bedline()
        #self.init_graph_labels()
        return rs_not_found
    
    def read_loci_array(self,BIM_file,rs_location_file=None,rs_file_cols=None):
        """ 
        Read locations of SNP ids from BIM file
        If rs_location_file and rs_file_cols are specified, read in this file as updated location
        rs_file_cols must specify columns in rs_location_file for chrom,bp_position, prbCode and rs
        """
        all_snps = numpy.genfromtxt(open(BIM_file, 'rb'), usecols=(0,1,3),
                                dtype={'names': ['chrom','prbCode','bp_position'],
                                       'formats':['S16', 'S16', int]})
        #self.snp = all_snps[self.idx2snp-1]
        #self.rs = self.snp['rs']
        
        rs_not_found=[]  # list to store rs of snps that are not found
        if os.path.isfile(rs_location_file):  
            snp_location = numpy.genfromtxt(open(rs_location_file, 'rb'),                   
                         usecols=rs_file_cols, 
                         dtype={'names': ['chrom','bp_position','prbCode','rs'], 
                        'formats':['S16',int,'S16','S16']}) # column 4 is actually

            counter=0 
            for idx,snp in enumerate(self.snp):
                snp_details=all_snps[self.idx2snp[idx]-1] # details from bim file
                prbCode=snp_details['prbCode'] # update prbCode
                rs_row=snp_location[(snp_location['prbCode']==prbCode),:]
                self.snp['prbCode'][idx]=prbCode # reset in case not correct, but looks ok
                if len(rs_row) >1:
                    print ('RS location file contains more than one entry for %s' % rs) 
                if len(rs_row) is not 0:            
                    self.snp[idx]['rs'] = rs_row['rs']
                    self.snp[idx]['chrom'] = rs_row['chrom'][0].split('chr')[1]
                    self.snp[idx]['bp_position'] = rs_row['bp_position']
                else: 
                    print(idx,prbCode)
                    rs_not_found.append(prbCode)
                    counter=counter+1

            print('Number of snps not found is %d'  % counter)

        self._init_chroff()
        self._init_bedline()
        self.init_graph_labels()
        return rs_not_found


    def read_gene_names(self,snp_bed_file,config,left_window=0,right_window=0):
        """
        calls find_gene_intersection which uses bedtools intersection 
         return an array containing:
        - snp chrom, snp start, snp stop, rs number 
        - gene chrom, gene start, gene stop, gene name, gene strand
        - signed base pair distance, absolute base pair distance
        signed base pair distance is: 
        - values (distance of snp upstream of gene start site)
        + values (distance of snp downstream of gene end site)
        - assume for bedtools, all genomic coordinates are 'left to right' 
        - use strand information to determine whether snp lies 'upstream' or 'downstream'
        see tools_overlap.py: _gene_snp_distance to see algorithm for calculation
        create gene_list structured array 
        each row contains a tuple of values representing up to 3 genes mapping to the snp
        the tuple contains a 4 fields, each containing a 1*3 array
        the fields are gene (gene name), bp_distance (integer for absolute value of distance),  sigend bp_distance (string containing upstream,downstream information), gene strand (strand of gene)
        """
        self.gene_name = zeros(self.num_vertex,dtype={'names':['gene'],'formats':['S16']}) 

        dt = dtype=([('gene', '(1,3)S16'),('bp_distance','(1,3)int'),('signed bp_distance','(1,3)S16',),('strand', '(1,3)S16')])
        self.gene_list= zeros(self.num_vertex,dtype=dt) # each record contains two lists up to length 3
        
        #if windows are = 0, return gene, otherwise finds gene within left and right windows
        gene_array= find_gene_intersection(snp_bed_file,config,left_window,right_window) 
        
        for idx,rs in enumerate(self.snp['rs']):
            row=gene_array[gene_array['rs']==rs,]
            if row.size==0: # row is empty
                            # do nothing
                #self.gene_name[idx]=zeros(0) # empty string
                self.gene_name['gene'][idx]='' # empty string
                self.gene_list['gene'][idx]='' # empty string
                #data.gene_list['bp_distance'][idx]=' ' # empty string
                #data.gene_list['direction'][idx]=' ' # empty string
            else: # row is not empty
                delimiter=', '
                genes=delimiter.join(unique(row['gene']))
                self.gene_name['gene'][idx]=genes
                #tuplelist=[row['gene'],row['distance']]
                row.sort(order='absolute distance')
                for i in range(len(row['gene'])):
                    if i > 2:
                        break # only store up to 3 genes
                    self.gene_list['gene'][idx][0][i]=row['gene'][i]
                    self.gene_list['bp_distance'][idx][0][i]=row['absolute distance'][i]
                    self.gene_list['signed bp_distance'][idx][0][i]=row['distance'][i]
                    self.gene_list['strand'][idx][0][i]=row['gene strand'][i]
                    #self.gene_list['direction'][idx][0][i]=row['direction'][i]
                

    def _init_snp2idx(self, snp1, snp2):
        """Construct the hashes to translate between snp id and graph vertex id"""
        self.idx2snp = sort(unique(hstack([snp1, snp2])))
        self.num_vertex = len(self.idx2snp)
        self.subgraph_id = zeros(self.num_vertex)
        num_snp = max([max(snp1),max(snp2)])+1 
        self.snp2idx = zeros(num_snp, dtype=int)
        for idx,snp in enumerate(self.idx2snp):
            self.snp2idx[snp] = idx

    def _init_chroff(self):
        """Construct the string for each SNP corresponding to chromosome and bp_position"""
        self.chroff = zeros(self.num_vertex, dtype=(numpy.str_,16))
        for idx, elem in enumerate(self.snp):
            self.chroff[idx] = 'chr%02s-%dk' % (elem['chrom'], round(elem['bp_position']/1000))

    def _init_bedline(self):
        """Construct the string for each SNP corresponding to a
        line in a UCSC BED file, suitable for BedTools.
        """
        self.bedline = zeros(self.num_vertex,
        dtype={'names':['chrom','chromStart','chromEnd','name','prb','score'],
               'formats':['S5', int, int, 'S16',int, float]})
        for idx, elem in enumerate(self.snp):
            self.bedline[idx] = ('chr%s' % elem['chrom'], elem['bp_position']-1, elem['bp_position'],
                                 elem['rs'],elem['prb'], 0.0)

    def export_snp_list(self,filename):
        """ used to export all snps in pairs for ld or block calculation and analysis in plink or haploview"""    
        snp_pairs=open('%s.pairs' % filename, 'wb')
        numpy.savetxt(snp_pairs,self.snp['rs'],fmt="%s")
        snp_pairs.close()

    def init_LD_graph_sparse(self,ld='r2',sparse=True,hdf5_file=None,PLINKdir=None,probe_dtype=None):
        """
        Construct the weighted graph from the snp pairs. 
        Graph features are:
        - LD_mat: adjacency matrix
        """
        LD_mat = lil_matrix((self.num_vertex,self.num_vertex))
        # set all to 0.    
        if sparse==True: 
            pairs=nonzero(self.adj_mat)
            for idx in range(len(pairs[0])):
                idx1=pairs[0][idx]
                idx2=pairs[1][idx]
                if idx1 < idx2:
                    r=calc_LD(self,idx1,idx2,ld,hdf5_file=hdf5_file,PLINKdir=PLINKdir,probe_dtype=probe_dtype) 
                    LD_mat[idx1, idx2] = r 
                    LD_mat[idx2, idx1] = r
        else:
            print("non-sparse LD mat not implemented")
        self.assoc['LD_mat'] = LD_mat
 
    def init_graph_labels(self):
        """For visualization, give the edges and vertices names"""
        exec('self.vertex_names = self.'+self.vertex_name)
        self.edge_names = self.adj_mat
        
    def init_networkx(self, min_vertex=1, max_vertex=numpy.inf):
        """Construct the networkx Graph, remove subgraphs with less than min_vertex vertices"""
        filter = False
        if min_vertex>1 and max_vertex<numpy.inf:
            (colours, counts) = self.find_large_components(min_vertex)
            medium = flatnonzero(array(counts) < max_vertex)
            colours = colours[medium]
            filter = True
            print('Only exporting %d graphs between size %d and %d'\
                % (len(colours), min_vertex, max_vertex))

        for idx, vert in enumerate(self.vertex_names):
            if filter:
                if self.subgraph_id[idx] not in colours: continue
            self.graph.add_node(idx, label=vert, degree=self.diagonal_unweighted[idx])
            self.graph.node[idx]['chrom'] = self.snp[idx]['chrom']
            self.graph.node[idx]['rs'] = self.snp[idx]['rs']
            self.graph.node[idx]['bp_position'] = self.snp[idx]['bp_position']
            self.graph.node[idx]['subgraph_id'] = self.subgraph_id[idx]
        for idx1 in range(self.num_vertex):
            if filter:
                if self.subgraph_id[idx1] not in colours: continue
            for idx2 in range(idx1, self.num_vertex):
                if self.adj_mat[idx1,idx2] > 0.0:
                    self.graph.add_edge(idx1, idx2, weight=self.adj_mat[idx1, idx2],
                                        subgraph_id=self.subgraph_id[idx1],
                                        community_id=self.community_id[idx1, idx2])




    def graph_statistics(self, min_count=20):
        """Some simple statistics about the graph"""
        super(SnpGraph, self).graph_statistics()
        
        print('Hubs that interact with more than %d pairs' % min_count)
        for idx in xrange(self.num_vertex):
            if self.diagonal_unweighted[idx]>min_count:
                print('snp=%d, chroff=%s, count=%d' %\
                    (self.idx2snp[idx], self.chroff[idx], self.diagonal_unweighted[idx]))

    def bp_distance(self):
        """Return the distance, in base pairs, between SNPs.
        SNPS from different chromosomes have distance -1.
        Unconnected SNPs have distance Nan.
        """
        distance = zeros((self.num_vertex, self.num_vertex))
        for snp1 in range(self.num_vertex):
            for snp2 in range(snp1,self.num_vertex):
                if snp1 == snp2:
                    continue
                if self.adj_mat[snp1,snp2] > 0.0:
                    if self.snp[snp1]['chrom'] == self.snp[snp2]['chrom']:
                        distance[snp1,snp2] = abs(self.snp[snp1]['bp_position']
                                                  - self.snp[snp2]['bp_position'])
                        distance[snp2,snp1] = abs(self.snp[snp1]['bp_position']
                                                  - self.snp[snp2]['bp_position'])
                    else:
                        distance[snp1,snp2] = -1.
                        distance[snp2,snp1] = -1.
                else:
                    distance[snp1,snp2] = numpy.nan
                    distance[snp2,snp1] = numpy.nan
        return distance

    def connected_same_chromosome(self):
        """Return the edges which are from the same chromosome
        """
        same = zeros((self.num_vertex, self.num_vertex), dtype=bool)
        for snp1 in range(self.num_vertex):
            for snp2 in range(snp1,self.num_vertex):
                if (self.snp[snp1]['chrom'] == self.snp[snp2]['chrom']
                    and self.adj_mat[snp1,snp2] > 0.0):
                    same[snp1,snp2] = True
        if same.any():
            return numpy.nonzero(same)
        else:
            return None

    def extract_subgraph(self, colour):
        """Construct a subgraph with the given colour,
        and return it
        """
        subgraph = SnpGraph('subgraph_%02d' % colour)
        vertices = flatnonzero(self.subgraph_id == colour)
        subgraph = super(SnpGraph, self).extract_subgraph(colour)
        subgraph.__class__ = SnpGraph
        subgraph.snp = self.snp[vertices]
        subgraph.chroff = self.chroff[vertices]
        subgraph.rs = self.rs[vertices]
        subgraph.bedline = self.bedline[vertices]
        subgraph.idx2snp = self.idx2snp[vertices]
        subgraph.snp2idx = self.snp2idx[vertices]
        subgraph.assoc=self.assoc
        # Not clear why gene name is in SnpGraph
        #subgraph.gene_name = self.gene_name[vertices]
        #subgraph.gene_list = self.gene_list[vertices]
        return subgraph

    def extract_snp_subgraph(self, snp_id):
        """Construct a subgraph for snp_id,
        and return it
        """
        subgraph = SnpGraph('snp_subgraph_%02d' % snp_id)
        subgraph = super(SnpGraph, self).extract_snp_subgraph(snp_id)
        vertices=flatnonzero(self.adj_mat.toarray()[snp_id,:])
        vertices=vertices.tolist()
        vertices.append(snp_id) # append snp_id 
        subgraph.__class__ = SnpGraph
        subgraph.snp = self.snp[vertices]
        subgraph.chroff = self.chroff[vertices]
        subgraph.rs = self.rs[vertices]
        subgraph.bedline = self.bedline[vertices]
        subgraph.idx2snp = self.idx2snp[vertices]
        subgraph.snp2idx = self.snp2idx[vertices]
        subgraph.assoc=self.assoc
        return subgraph

    def extract_bipartite_subgraph(self, clus_1,clus_2,vertices):
        """Construct a subgraph between snps(vertices) in clus_1 and clus_2,
        and return it. Pass in vertices belonging to clusters. (clus_1, and 2 used for naming only)
        """
        subgraph = SnpGraph('bipartite_subgraph_%02d_%02d' % (clus_1,clus_2))
        subgraph = super(SnpGraph, self).extract_bipartite_subgraph(clus_1,clus_2,vertices)
        #vertices=flatnonzero(self.bedline['clus_id']==clus_1 | data.bedline['clus_id']==clus_2)
        #vertices=vertices.tolist()
        subgraph.__class__ = SnpGraph
        subgraph.snp = self.snp[vertices]
        subgraph.chroff = self.chroff[vertices]
        subgraph.rs = self.rs[vertices]
        subgraph.bedline = self.bedline[vertices]
        subgraph.idx2snp = self.idx2snp[vertices]
        subgraph.snp2idx = self.snp2idx[vertices]
        subgraph.gene_name = self.gene_name[vertices]
        subgraph.gene_list = self.gene_list[vertices]
        subgraph.assoc=self.assoc
        return subgraph



    def export2bed(self, filename, delimiter='\t', topline=False):
        """Write each SNP as a line in a BED file"""
        outfile = open(filename, 'wb')
        if topline:
            outfile.write(delimiter.join(self.bedline.dtype.names) + '\n')
        for line in self.bedline:
            outfile.write(delimiter.join(map(str, line)) + '\n')
        outfile.close()


    def export2sif(self, filename, delimiter='\t', topline=False):
        """Write each SNP-SNP interaction as a line in a SIF file
        for export to Cytoscape"""
        outfile = open(filename, 'w') # writing text file for read into Cytoscape
        adj_array=self.adj_mat.toarray() # numpy array
        adj_array_upper=triu(adj_array) #avoid including interactions twice
        ind=nonzero(adj_array_upper) # tuple containing indicies of nonzero elements
        row,col=ind # unpack tuple
        sifline = zeros(len(row),
                              dtype={'names':['snp1','type','snp2'],
                                     'formats':['S16','S16','S16']})
        for i in range(len(row)): # length of first element is
            sifline['snp1'][i]=self.rs[row[i]]  # row[i]
            sifline['type'][i]='epistasis'
            sifline['snp2'][i]=self.rs[col[i]]  # col[i]
        for i,line in enumerate(sifline):
            outfile.write(delimiter.join(map(str, line)) + '\n')
        outfile.close()

    def exportNodeAttributes(self, filename, delimiter='=', topline=False):
        """Write each SNP as a line in a Node Attribute file
        for export to Cytoscape"""
        outfile = open(filename, 'w') # writing text file for read into Cytoscape
        NAline = zeros(self.num_vertex,
                              dtype={'names':['rs_identifier','GeneSymbol','distance','strand'],
                                     'formats':['S16','S32','S32','S32']})
        for i,rs in enumerate(self.snp['rs']): # length of first element is
            NAline['rs_identifier'][i]=rs  # row[i]
            NAline['GeneSymbol'][i]=str(self.gene_list[i][0][0])  # col[i]
            NAline['distance'][i]=str(self.gene_list[i][2][0])  # col[i]
            NAline['strand'][i]=str(self.gene_list[i][3][0])  # col[i]
        outfile.write(NAline.dtype.names[1] + '\n')
        for i,line in enumerate(NAline):
            outfile.write(delimiter.join(map(str, line)) + '\n')
        
        outfile.close()


class GeneGraph(SnpGraph):
    """A container for representing the graph of interactions.
    Each vertex is a cluster of SNPs
    """
    def __init__(self, name, vertex_name='chroff', bonferroni=None):
        """filename is a string to the bed file"""
        super(GeneGraph, self).__init__(name, vertex_name, bonferroni)
        self.bipartite_mat=lil_matrix(0)

    def __repr__(self):
        """The name"""
        return '<GeneGraph(%s)>' % self.name

    def read_bedfile(self, filename=None, delimiter='\t'):
        """Read BED file format, coming from clustered SNPs"""
        if filename is not None:
            infile = open(filename, 'r')
        else:
            infile = open(self.filename, 'r')
        bedline_raw = []
        for line in infile:
            bedline_raw.append(line[:-1])
        infile.close()
        self.num_vertex = len(bedline_raw)
        self.bedline = zeros(self.num_vertex,
            dtype={'names':['chrom','chromStart','chromEnd','name','prb','score','clus_id'],
                   'formats':['S5', int, int, 'S16',int, float, int]})
        for i, line in enumerate(bedline_raw):
            split_line=tuple(line.split('\t'))
            probe=int(split_line[4]) # terrible hard-coding of probe index in line.
            idx=self.snp2idx[probe] #
            self.bedline[idx] = split_line

        if 0 in self.bedline['prb']:
            print('there is a probe value that was not found')
            exit

        #self.init_bipartite_matrix()

    def init_bipartite_matrix(self):
        """ A bipartite matrix is an adjacency matrix
            type: lil_matrix
            size: num_clusters by num_clusters
            content: number of edges between two clusters
            Ensure a bedline exists with a cluster id
            Note - may be a faster way to do this,
            - could find all unique cluster-cluster pairs matching snp-snp pairs, 
            - and only fill in non-zero values """
        #if self.bedline.shape[0]==0: # not initialised - need to fix
            #print 'bedline not initialised'   
        num_clus=len(unique(self.bedline['clus_id']))
        print('num_clusters is %s', num_clus)
        clusters=unique(self.bedline['clus_id'])
        self.bipartite_mat=lil_matrix((num_clus,num_clus)) # convert this to object, and put in own function 
        # retain only those values for clusters where there is a non zero entry for snp-snp pair
        # for each snp-snp pair that is not zero

        
        pairs=nonzero(self.adj_mat)
        for idx in range(len(pairs[0])): # each index
            clus_id1=self.bedline['clus_id'][pairs[0]][idx]
            clus_id2=self.bedline['clus_id'][pairs[1]][idx]
            if(self.bipartite_mat[clus_id1-1,clus_id2-1]==0): # if not yet filled
                if (clus_id1 < clus_id2): # diagonal should be zero   
                    num_edges=0 #    
                    vertices_i=flatnonzero(self.bedline['clus_id']==clus_id1)            
                    vertices_j=flatnonzero(self.bedline['clus_id']==clus_id2)
                    bi_adj_mat=self.adj_mat[vertices_i,:][:,vertices_j]
                    num_edges=len(nonzero(bi_adj_mat)[0]) # no. of nonzero edges
                    self.bipartite_mat[clus_id1-1,clus_id2-1]=num_edges
                    self.bipartite_mat[clus_id2-1,clus_id1-1]=num_edges

       #for clus_idx_i in range(num_clus): # for each cluster
       #    clus_i=clus_idx_i+1 
       #    vertices_i=flatnonzero(self.bedline['clus_id']==clus_i)
       #    for clus_idx_j in range(num_clus):
       #        clus_j=clus_idx_j+1
       #        num_edges=0 # 
       #        if (clus_idx_i <= clus_idx_j):                     
       #             vertices_j=flatnonzero(self.bedline['clus_id']==clus_j)
       #            bi_adj_mat=self.adj_mat[vertices_i,:][:,vertices_j]
       #            num_edges=len(nonzero(bi_adj_mat)[0]) # no. of nonzero edges
       #            ##for snp_id_i in vertices_i:
       #                ##for snp_id_j in vertices_j:
       #                    ##if self.adj_mat[snp_id_i,snp_id_j] > 0.0: # GWIS edge
       #                        ##print 'edge found %d %d' % (clus_i,clus_j)
       #                        ##num_edges=num_edges+1 # count number of edges
       #                        ##print num_edges
       #            self.bipartite_mat[clus_idx_i,clus_idx_j]=num_edges
       #            self.bipartite_mat[clus_idx_j,clus_idx_i]=num_edges

    def extract_subgraph(self, colour):
        """Construct a subgraph with the given colour,
        and return it
        """
        subgraph = GeneGraph('subgraph_%02d' % colour)
        vertices = flatnonzero(self.subgraph_id == colour)
        subgraph = super(GeneGraph, self).extract_subgraph(colour)
        subgraph.__class__ = GeneGraph
        subgraph.gene_name = self.gene_name[vertices]
        subgraph.gene_list = self.gene_list[vertices]
        subgraph.bedline = self.bedline[vertices]
        return subgraph

    def extract_snp_subgraph(self, snp_id):
        """Construct a subgraph with the given colour,
        and return it
        """
        subgraph = GeneGraph('snp_subgraph_%02d' % snp_id)
        vertices=flatnonzero(self.adj_mat.toarray()[snp_id,:])
        vertices=vertices.tolist()
        vertices.append(snp_id) # append snp_id  # do you need above 3 lines?
        subgraph = super(GeneGraph, self).extract_snp_subgraph(snp_id)
        subgraph.__class__ = GeneGraph
        subgraph.bedline = self.bedline[vertices]
        subgraph.gene_name = self.gene_name[vertices]
        subgraph.gene_list = self.gene_list[vertices]
        return subgraph

    def extract_bipartite_subgraph(self, clus_1,clus_2):
        """Construct a subgraph between snps(vertices) in clus_1 and clus_2,
        and return it
        """
        subgraph = GeneGraph('bipartite_subgraph_%02d_%02d' % (clus_1,clus_2))
        vertices=flatnonzero(self.bedline['clus_id']==clus_1).tolist() + flatnonzero(self.bedline['clus_id']==clus_2).tolist()
        subgraph = super(GeneGraph, self).extract_bipartite_subgraph(clus_1,clus_2,vertices)
        subgraph.__class__ = GeneGraph
        subgraph.bedline = self.bedline[vertices]

        return subgraph

    def find_bipartite_subgraph(self,min_edges=1):
        """ return cluster ids of cluster pair where pair has 
            more than min_edges number of edges between cluster 1  and cluster 2"""
        large_bipartite= triu(self.bipartite_mat.toarray()>min_edges)
        return nonzero(large_bipartite)[0]+1,nonzero(large_bipartite)[1]+1 # returning a tuple containing clusters (not index)

	# can update this function to have following pseudo-code:
	# for each cluster, 
		# get LD of 2 nodes (LD_mat now stores LD for ALL nodes)
		# if LD_mat(pair) > threshold merge
		# OR if density(pair) > 1 
			# merge two nodes, can use a breadth-first tree-like 

    def merge_vertices(self):
        """Merge all vertices in the same cluster"""
        clus_id = []
        for snp in self.bedline:
            clus_id.append(snp['clus_id'])
        clus_id = sort(unique(array(clus_id)))
        if len(clus_id) == 1:
            print('Only one cluster, not merging')
            return
        for clus in clus_id:
            cur_lines = flatnonzero(self.bedline['clus_id'] == clus)
            self._merge_vertices(cur_lines)

    def _merge_vertices(self, vertices):
        """Merge vertices given in 'vertices'"""
        edges = self._edge_boundary(vertices)
        add_edges=[]
        for (u,v,l) in edges:
            if v in vertices and v != vertices[0]:
                add_edges.append((vertices[0],u,l))
            if u in vertices and u != vertices[0]:
                add_edges.append((vertices[0],v,l))
        self.graph.add_weighted_edges_from(add_edges)
        self.graph.remove_nodes_from(vertices[1:])

    def _edge_boundary(self, vertices):
        """Return the edges that are from 'vertices' to outside"""
        neighbors = []
        for vert in vertices:
            neigh = self.graph.neighbors(vert)
            for cur_neigh in neigh:
                # multigraph
                #weight = self.graph[vert][cur_neigh][0]['weight']
                # graph
                weight = self.graph[vert][cur_neigh]['weight']
                neighbors.append((vert, cur_neigh, weight))
        return neighbors


