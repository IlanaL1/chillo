"""Create a pythonic interface to command line GWIS"""

import os
import subprocess
from numpy import loadtxt, flatnonzero, hstack
import qsub

def process(data_name, k, input_dir=None, output_dir=None, bonferroni=0.0, hypo_test=['Chi2'],
            verbose=False):
    """Check_call gwis to retrieve the top k putative SNP pairs"""
    res_filename = call(data_name, k, input_dir, output_dir, bonferroni, hypo_test, verbose=verbose)
    pairs, frac_bonf = get_pairs(res_filename, bonferroni, verbose)
    return pairs, frac_bonf

def call(data_name, k, input_dir=None, output_dir=None, bonferroni=0.0, hypo_test=['Chi2'],
         cluster=False, num_hours=12, num_threads=8, gpu=False, pairs_file=None, verbose=False):
    """Check_call gwis
    returns the list of file names that each contain a list of pairs.
    """
    # Setup command string
    if gpu:
        command_str = 'gwis_gpu '
    else:
        command_str = 'gwis_cpu '
    if verbose:
        command_str += '--verbose '

    command_str += '--dataset %s ' % data_name
    command_str += '--scores %d ' % k
    if not gpu:
        command_str += '--threads %d ' % num_threads
    
    if input_dir is not None:
        command_str += '--input-path %s ' % input_dir
    if output_dir is not None:
        command_str += '--output-path %s ' % output_dir
        output_name =  '%s/%s_sel' % (output_dir, data_name)
    else:
        output_name = '%s_sel' % data_name

    res_filename = []
    for stat in hypo_test:
        res_filename.append('%s_%s.txt' % (output_name, stat))
        command_str += '--test:%s ' % stat
    if hypo_test == 'GSS' and pairs_file is not None:
        command_str += '--snp-pairs %s ' % (pairs_file)

    # Actual computation done here
    if cluster:
        print('qsub %s' % command_str)
        header = qsub.gen_header('gwis', 'serial', num_hours, num_threads)
        #header = qsub.gen_header_massive('gwis')
        qsub.submit(header, command_str+'\n')
    else:
        subprocess.check_call(command_str, shell=True)

    return res_filename

def get_pairs(res_filename, bonferroni=0.0, verbose=False):
    """Process the resulting pairs"""
    data = get_scores(res_filename)
    pairs, scores = data[:,:2].astype(int), data[:,2]
    k = len(scores)
    sig_scores = flatnonzero(scores > bonferroni)
    frac_bonf = float(len(sig_scores))/float(k)
    if verbose:
        print('%d of %d scores are above Bonferroni correction' % (len(sig_scores), k))
    return pairs, frac_bonf

def get_scores(res_filename):
    """Load the pairs and scores"""
    data = loadtxt(res_filename, delimiter='\t', skiprows=4)
    #pairs = data[:,:2].astype(int)
    #scores = data[:,2]
    return data

def cleanup(data_name, output_dir, hypo_test):
    if output_dir is None:
        output_dir = ''
    os.remove('%s/%s_exec.txt' % (output_dir, data_name))
    os.remove('%s/%s_meta.txt' % (output_dir, data_name))
    for stat in hypo_test:
        os.remove('%s/%s_profile_%s.txt' % (output_dir, data_name, stat))
        os.remove('%s/%s_sel_%s.txt' % (output_dir, data_name, stat))


