import sys
sys.path.append('/home/ilana/Documents/epistasis/GraphEnrich')
from chillo.tools_config import configure_from_file
from chillo.snp_graph import SnpGraph, GeneGraph
from chillo.tools_plink import count_lines
from chillo.io_gwis import read_gss_file, all_snps_from_array, create_snps_array,update_array

#from snp_graph import SnpGraph # initialising a GeneGraph subclass
#from gene_graph import GeneGraph
#from config_default import configure_from_file # will move to new location
import os
from chillo.tools_overlap import snp_region_intersection
from chillo.tools_summary_plots import draw_heatmap, plot_hist,plot_bp_vs_ld,plot_bp_vs_gwis
from chillo.cluster_locations import find_clusters
from chillo.check_genes import find_gene_statistics 
from chillo.tools_GSEA import gene_set_analysis, gene_set_pair_analysis, plot_gene_results
from chillo.write_ndarray_results import write_results_file
from chillo.view_subgraphs import plot_clustered_subgraph, plot_large_components, plot_large_components_LD, plot_large_components_gene_name, plot_large_components_bp_distance, view_bipartite_subgraph, view_subgraph_rs, view_bipartite_subgraph_rs, summarise_bipartite_subgraphs,summarise_hub_subgraphs, plot_large_bipartite_subgraph
   

from chillo.io_pickle import load, save

def process_datasets(config):
    """assumes directory structure data_dir/conditionWTC
     calls process for each condition"""
    for disease in config['datasets']:
        plink_prefix='%s%s/%sWTC' % (config['genotype_dir'],disease, disease)
        if os.path.isfile('%s.bim' % plink_prefix):
            pickle_filename='%s_%s.pkz' % (config['pickle_filename'],disease)    
            if not os.path.isfile('%s%s/%s' % (config['output_dir'],disease,pickle_filename)):
                create_dataset(disease, config,pickle_filename)
    
            analyse_dataset(disease,config,pickle_filename)
        else:
            print('Usage: directory should contain PLINK files with prefix condition+WTC e.g bdWTC')
            exit(1)


def create_dataset(disease,config,output_file):
    """
    Read the data for a disease contained in filename, which should be the output
    of a GSS step in the GWIS pipeline.
    Construct the SnpGraph
    Save the SnpGraph through pickle
    """
    filters = ['fltGSS_prtv', 'fltGSS', 'fltGSS_cntr', 'fltSS', 'fltDSS', 'fltChi2']
    #filters=config['filters']
    filename=config['gwis_file']

    if("/" in filename): 
        expt_name = disease+" "+filename.split('/')[-1]
    else:
        expt_name = disease+" "+filename

    # change directory to results dir
    results_dir=('%s%s' % (config['output_dir'],disease))
    if os.path.isdir(results_dir): # os.getcwd() gives current working directory
        os.chdir(results_dir) # change into subdirectory e.g. bd
    else:
        print('Usage: please specify in config file top level directory for WTCCC results')
        exit(1)

    # initialise GeneGraph
    data = GeneGraph(expt_name)


    # do we still need bim file to get correct locations?
    plink_prefix='%s%s/%sWTC' % (config['genotype_dir'],disease, disease)
    bim_file = plink_prefix + '.bim' # BIM file
    num_snps = count_lines(bim_file) # number of rows (interactions)

    # default is to assume negative log p value if stat is not recognised
    data.set_bonferroni('fltDSS',num_snps,bonferroni=0) # choose stat 



    # readin GSS data
    raw_data = read_gss_file(filename) #return a structured array with all information 
    idx=raw_data['study']==disease
    disease_data=raw_data[:][idx]
    disease_data.sort(order='fltDSS')
    disease_data=disease_data[::-1]
    # keep only rows above bonferroni
    disease_data=disease_data[disease_data['fltDSS']>data.bonferroni,:]
   
    probe_list=all_snps_from_array(disease_data)
    # rs_file_cols should be order of chrom, bp_position,prbCode,rs 
    # if prbCode==rs, just enter the same column number twice
    filtered_snp_array,probe_not_found=create_snps_array(probe_list,bim_file,config['rs_map'], rs_file_cols=(1,3,4,8))
    filtered_disease_data=update_array(disease_data,probe_not_found)

    data.init_from_sarray(filtered_disease_data,filtered_snp_array,filters)

    #data.init_from_sarray(disease_data, filters)
    data.set_adj_mat('fltGSS')
    stat='fltGSS'

    # pairs_not_found contains snps for pairs not found in RS_LOCATION flat file.
    # rs_file_cols should be order of chrom, bp_position,prbCode,rs    
    #pairs_not_found = data.read_loci_array(bim_file, config['rs_map'], rs_file_cols=(1,3,4,8))
    #  1 snp not found in bd



    print("export snps as bed file")
    out_file=('%s_bedfile') % (disease) # no need for stat
    data.export2bed('%s.bed' % out_file)
    snp_bed= '%s.bed' % out_file 
        
    # Find clusters of snps based on bp_distance using bedtools
    print("find clusters")    
    find_clusters(data,snp_bed)    # cluster BED file based on bp distance... 
    data.read_bedfile('clustered.bed') # read in cluster information

    print("create adj_matrix of clusters")    
    data.init_bipartite_matrix()

    print("read gene names")
    #data.read_gene_names(snp_bed,config,left_window=2000,right_window=500) # windows from dbSnp FAQ
    data.read_gene_names(snp_bed,config,left_window=100000,right_window=100000) 

    print("initialise LD matrix")
    hdf5_file=config['WTCCC_PATH'] + disease + '/' + disease+'WTC_mat.h5'
    data.init_LD_graph_sparse(ld='r2',sparse=True,hdf5_file=hdf5_file)

    #PLINKdir='%s%s/%sWTC' % (config['genotype_dir'],disease, disease)
    #probe_dtype='prbCode'
    #data.init_LD_graph_sparse(ld='r2',hdf5_file=None,PLINKdir=PLINKdir,probe_dtype=probe_dtype)# 

    save(output_file, data)
    return

def analyse_dataset(disease,config,input_file):

    # change directory to results dir
    results_dir=('%s%s' % (config['output_dir'],disease))
    if os.path.isdir(results_dir): # os.getcwd() gives current working directory
        os.chdir(results_dir) # change into subdirectory e.g. bd
    else:
        print('Usage: please specify in config file top level directory for WTCCC results')
        exit(1)


    data=load(input_file)
    stat='fltGSS'

    # plot heatmap of pairs with genomic coordintes showing r2 as colourbar. 
    # can fix later so only show pairs within 1MB of eachother (less distracting)
    print("plot summary heatmap and histogram")    
    #draw_heatmap(data,disease,stat)
    
    # plot histogram
    #plot_hist(data,disease,stat)        
    
    # plot bp distance vs ld
    #plot_bp_vs_ld(data,disease,stat) #    

    # plot bp distance vs ld
    #plot_bp_vs_gwis(data,disease,stat) # 



    print("summarise bipartite subgraphs")
    bipartite_results=summarise_bipartite_subgraphs(data)
    write_results_file('bipartite_subgraph_summary_%s_%s.csv' % (disease,stat), bipartite_results,topline=True)
    hub_results=summarise_hub_subgraphs(data)
    write_results_file('hub_subgraph_summary_%s_%s.csv' % (disease,stat), hub_results,topline=True)

    
    #print("plot top 5 bipartite subgraphs")
    #for i in range(5):
     #   result=bipartite_results[i]
      #  cluster_edges=result['cluster IDs']
        #view_bipartite_subgraph(data,cluster_edges[0],cluster_edges[1],'%s_%s' % (disease,stat))
                
    #print("plot top 5 hub subgraphs")
    #for i in range(5):
     #   result=hub_results[i]
      #  snp_id=result['hub ID']
       # rs_id=data.snp['rs'][snp_id]
        #view_subgraph_rs(data,rs_id,'%s_%s' % (disease,stat),hub_only=True)
        
    print("find gene statistics")
    #find_gene_statistics(data,snp_bed,disease,stat,config)  # calls check_genes function classify_loci - currently causing bugs

    # read in pathway
    #concurrence_results=compute_concurrence(data,config) # compute concurrence scores

    print("data.colour_connected")
    #data.colour_connected()
               
    print("plot large components")
    # not plotting now because too slow
    #plot_large_components(data, disease,config['stat']) #snp2graph functions, filename needed for calling ld
    #plot_large_components_LD(data, disease,stat,hdf5file=hdf5_file,PLOT_R2=True)
    #plot_large_components_gene_name(data, disease,stat)
    #plot_large_components_bp_distance(data, disease,stat)
    
    #print("plot_clustered subgraphs")        
    #plot_clustered_subgraph(data, 2, '%s_%s' % (disease,stat)) 

    # plot all bipartite subgraphs with more than 3 edges
    print('plot bipartite subgraphs')
    #plot_large_bipartite_subgraph(data,'%s_%s' % (disease,stat),3) # min_edges=3, # function needs to be debugged

    print('plot hub subgraphs')
    # not written

    print("gene set analysis")
    for gene_sets in os.listdir(config['MSigDB_dir']):
        print('processing %s' % gene_sets)
        gene_sets_loc='%s/%s' % (config['MSigDB_dir'],gene_sets)        
        if os.path.isfile(gene_sets_loc) and gene_sets.split('.')[-1] == 'gmt':
            results=gene_set_analysis(data.gene_list['gene'],gene_sets_loc)
            genes_out_file='%s_%s_genes_overlap_%s' % (disease,stat,gene_sets)
            write_results_file(genes_out_file,results)
            #plot_gene_results(results,disease,stat,gene_sets,n=10) 
            
            results_pair=gene_set_pair_analysis(data,gene_sets_loc,GSEA_test='False')
            pairs_out_file='%s_%s_pairs_overlap_%s' % (disease,stat,gene_sets)
            write_results_file(pairs_out_file,results_pair)


   
    #export to sif and attribute files for cytoscape
    data.export2sif('%s_%s.sif' % (disease,stat))
    #data.exportNodeAttributes('%s_%s.NA' % (disease,stat)) # fix to use data.gene_list
    # cleanup
    os.remove('clustered.bed')
    os.remove('%s.bed' % out_file)
    del data

    os.chdir("..") # change out of disease 


if __name__ == '__main__':
    import os, sys
    if len(sys.argv) != 2:
        print 'Usage: python %s config.py' % sys.argv[0]
        exit(1)
    config = configure_from_file(sys.argv[1])
    process_datasets(config)



