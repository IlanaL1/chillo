
datasets = ['t1d','t2d','ht','cd','cad','ra','bd']
#datasets=['t2d']
#stats=['SS','Chi2']
#stats=['GSS']
stats=['fltGSS_prtv']

DATA_DIR = '/home/ilana/Documents/UCSC/'
BUILD = 'hg19'
ANNO_GENES = DATA_DIR + BUILD + 'genes.bed.gz'
ANNO_CODING = DATA_DIR + BUILD + 'coding.bed.gz'
ANNO_EXONS = DATA_DIR + BUILD + 'exons.bed.gz'
ANNO_INTRONS = DATA_DIR + BUILD + 'introns.bed.gz'
ANNO_MIRNA = DATA_DIR + BUILD + 'sno_miRNA.bed.gz'
ANNO_REFFLAT = DATA_DIR + BUILD + 'refFlat.bed.gz'	

# PLINK bed, bim, fam files
#genotype_dir = '/Users/cheng.ong/Data/WTCCC/'
genotype_dir = '/home/ilana/Documents/WTCCC/'

#output_dir = '/home/ilana/Documents/WTCCC/results_GSSPairs_Ben/'
#output_dir = '/home/ilana/Documents/WTCCC/results_GSSPairs_Feb13/'
#output_dir = '/home/ilana/Documents/WTCCC/results_GSSPairs_Cris/'
output_dir = '/home/ilana/Documents/WTCCC/results_GSSPairs_Adam13/' # latest file from Adam results

# Files from UCSC genome browser
ucsc_dir = DATA_DIR 
# File containing Affymetrix ID, RS number and chromosome basepair location
rs_map = ucsc_dir + 'snpArrayAffy250NspSty.txt'

# GWIS results
#gwis_dir = '/Users/cheng.ong/temp/results_gwis/'
#stat = 'Chi2'
#gwis_file='/home/ilana/Documents/epistasis/GraphEnrich/test/snp_sig_sel2w.txt'
#gwis_file='/home/ilana/Documents/epistasis/GraphEnrich/test/sel2w4Cris2.txt'
gwis_file='/home/ilana/Documents/WTCCC/GSSPairs_AdamFeb13/sel2w4Cris.txt' # latest file from Adam
#gwis_dir='/home/ilana/Documents/WTCCC/gwis_results'
#gwis_dir='/home/ilana/Documents/WTCCC/GSSPairs_Ben'

WTCCC_PATH='/home/ilana/Documents/WTCCC/'


#GSEA 
MSigDB_dir='/home/ilana/Documents/MSigDb/'

# pathway analysis
pathway = '/home/ilana/Documents/PathwayCommons_NCINature_SIF/'

# pickle object
pickle_filename='WTCCC' 
