
# This script produces JSON files from the dataset used in GWIS paper
# As cited in the GWIS platform paper. http://www.biomedcentral.com/content/supplementary/1471-2164-14-s3-s10-s2.xls

# to create the input file .txt open the 1471-2164-14-s3-s10-s2.xls
# and get copy and past the information of each columns below:
# rs_1	rs_2	chr_1	bp_1	chr_2	bp_2	fltChi2_1	fltChi2_2	fltGSS
# To do this for each disease and give this files like input to gwispaperdataset.py



 python gwispaperdataset.py  -i ../test/BD.txt -o BD_gwis_paper.json


 python gwispaperdataset.py  -i ../test/CAD.txt -o CAD_gwis_paper.json


 python gwispaperdataset.py  -i ../test/HT.txt -o HT_gwis_paper.json


 python gwispaperdataset.py  -i ../test/RA.txt -o RA_gwis_paper.json


 python gwispaperdataset.py  -i ../test/T1D.txt -o T1D_gwis_paper.json


 python gwispaperdataset.py  -i ../test/T2D.txt -o T2D_gwis_paper.json


 python gwispaperdataset.py  -i ../test/CD.txt -o CD_gwis_paper.json
