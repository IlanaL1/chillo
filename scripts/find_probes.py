"""A wrapper around some command line calls to
GWIS and PLINK"""

import os
import subprocess
import numpy
from chillo.tools_config import configure_from_file
import chillo.plink
import chillo.gwis
import chillo.qsub

have_qsub = True
try:
    subprocess.check_call('qsub --version', shell=True)
except subprocess.CalledProcessError:
    have_qsub = False
    print('Running jobs locally')


def run_gwis(config):
    if not os.path.isdir(config['result_dir']):
        os.makedirs(config['result_dir'])
    for disease in config['datasets']:
        if len(config['hypo_test']) > 0:
            print('GWIS %s' % disease)
            gwis_filename = chillo.gwis.call(disease, config['num_pairs'], config['data_dir'],
                                             config['result_dir'], cluster=have_qsub,
                                             num_threads=4, verbose=False,
                                             hypo_test=config['hypo_test'])
        for hypo_test in config['uni_test']:
            print('PLINK %s %s' % (disease, hypo_test))
            plink_filename = chillo.plink.call(disease, config['data_dir'], config['result_dir'],
                                               hypo_test=hypo_test, verbose=False)

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 2:
        print('Usage: python %s config.py' % sys.argv[0])
        exit(1)
    config = configure_from_file(sys.argv[1])
    run_gwis(config)
