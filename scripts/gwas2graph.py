import os
import numpy
from numpy import array, zeros, empty, vstack, hstack
from numpy import flatnonzero
from collections import defaultdict
from chillo.tools_config import configure_from_file
from chillo.snp_graph import SnpGraph
from chillo.tools_plink import count_lines
from chillo.io_gwis import read_gwis_file, all_snps_from_array, init_snps_array
from chillo.numpy_ext import find, unique_rows
from chillo.gwis import get_scores

def process_datasets(config):
    """calls process for each condition"""
    for disease in config['datasets']:
        filename='%s/%s' % (config['data_dir'], disease)
        if os.path.isfile('%s.bim' % filename):
            process(disease, config)
        else:
            print('Usage: directory should contain PLINK files with prefix condition+WTC e.g bdWTC')
            exit(1)

def collect_probe_info(config, dataset, bim_file):
    """Construct the list of all probes,
    found in both univariate and bivariate analysis,
    and get their information from the BIM file"""

    # TODO: collect univariate information

    probe_list = []
    for stat in config['hypo_test']:
        gwis_results = '%s/%s_sel_%s.txt' % (config['result_dir'], dataset, stat)
        print('Reading gwis results: %s' % gwis_results)
        raw_data = read_gwis_file(gwis_results, max_pairs=config['num_pairs'])
        print('%d pairs' % len(raw_data))
        probe_list.extend(all_snps_from_array(raw_data).tolist())
        print('Total: %d probes' % len(probe_list))
    probe_list = array(probe_list)
    probe_list.sort()
    print('Reading probe information from BIM file: %s' % bim_file)
    snp_array = init_snps_array(probe_list, bim_file)

    return snp_array

def collect_uni_info(config, snp_array, dataset):
    """Read PLINK results and add to snp_array"""
    for stat in config['uni_test']:
        plink_results = '%s/%s.assoc' % (config['result_dir'], dataset)
        if stat != 'assoc':
            plink_results += '.%s' % stat
        print('Reading plink results: %s' % plink_results)
        idx_sig, p_val = plink.get_probes(plink_results, stat, sig_level=0.05)
        # TODO: map idx_sig to snp_array
    return

def insert_score(raw_data, pair_array, stat):
    """For each row in raw_data, insert the score (column 2)
    into the element where
    the probe indices of pair_array match the indices in the row in raw data,
    and the column of pair array is stat.
    """
    if stat[:3] == 'GSS':
        raw_stat = 'GSS'
    else:
        raw_stat = stat    
    num_pairs = len(raw_data)
    for ix in range(num_pairs):
        idx1 = flatnonzero(raw_data['prb_1'][ix] == pair_array['prb_1'])
        idx2 = flatnonzero(raw_data['prb_2'][ix] == pair_array['prb_2'])
        idx = list(set(idx1).intersection(set(idx2)))[0]
        pair_array[stat][idx] = raw_data[raw_stat][ix]
    return

def collect_pair_info(config, dataset):
    """Read GWIS results and create an array of pairs"""
    header_names = ['prb_1','prb_2']
    header_names.extend(config['hypo_test'])
    header_formats = [int, int]
    header_formats.extend([float]*len(config['hypo_test']))

    all_data = empty((0,2), dtype=int)
    for stat in config['hypo_test']:
        gwis_results = '%s/%s_sel_%s.txt' % (config['result_dir'], dataset, stat)
        print('Reading gwis results: %s' % gwis_results)
        raw_data = read_gwis_file(gwis_results, max_pairs=config['num_pairs'])
        print('%d pairs' % len(raw_data))
        new_pairs = zeros((len(raw_data),2), dtype=int)
        new_pairs[:,0] = raw_data['prb_1']
        new_pairs[:,1] = raw_data['prb_2']
        all_data = vstack([all_data, new_pairs])
    all_data = unique_rows(all_data)

    pair_array = zeros(all_data.shape[0], dtype={'names': header_names,
                                                 'formats': header_formats})
    pair_array['prb_1'] = all_data[:,0]
    pair_array['prb_2'] = all_data[:,1]
    for stat in config['hypo_test']:
        gwis_results = '%s/%s_sel_%s.txt' % (config['result_dir'], dataset, stat)
        print('Reading gwis results: %s' % gwis_results)
        raw_data = read_gwis_file(gwis_results, max_pairs=config['num_pairs'])
        print('%d pairs' % len(raw_data))
        insert_score(raw_data, pair_array, stat)
        
    return pair_array

def process(dataset, config, community=False, roc=True):
    """ creates a SnpGraph object and reads in coordinates
    outputs JSON file"""

    geno_file = '%s/%s' % (config['data_dir'], dataset)
    bim_file = '%s/%s.bim' % (config['data_dir'], dataset)
    print('Processing %s' % dataset)
    data = SnpGraph(dataset)

    # edit if you want to set bonferroni.
    num_snps = count_lines(bim_file)
    data.set_bonferroni(config['hypo_test'][-1], num_snps, bonferroni=0)

    snp_array = collect_probe_info(config, dataset, bim_file)
    #collect_uni_info(snp_array)
    pair_array = collect_pair_info(config, dataset)

    print('Initializing %s' % data)
    data.init_from_sarray(pair_array, snp_array, config['hypo_test'])
    data.set_adj_mat(config['hypo_test'][-1])
    
    print('Finding connected subgraphs')
    data.colour_connected()
    data.sort_colours()
    data.count_edges_subgraphs()
    if community:
        print('Finding communities')
        data.find_community()
        data.count_edges_communities()
    print('Computing contingency tables')
    data.get_contingency_tables(geno_file)
    
    json_file = '%s/%s_%s.json' % (config['result_dir'],dataset, config['expt_name'])
    print('Writing to %s' % json_file)
    data.export_json(json_file, roc=roc)

if __name__ == '__main__':
    import os, sys
    if len(sys.argv) != 2:
        print('Usage: python %s config.py' % sys.argv[0])
        exit(1)
    config = configure_from_file(sys.argv[1])
    process_datasets(config)
