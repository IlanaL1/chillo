import csv
from numpy import array, zeros  

def read_breast_cancer_genes(filename):
    """ designed to read in csv file with columns that correspond to:
    column 0: chromosome, column 1:  start coordinate, column 2: end coordinate, column 6: gene name
    """

    with open(filename,'r') as f:
        lines=f.readlines()
        num_lines=len([l for l in lines])

    bc_gene_details=zeros(num_lines-1,
            dtype={'names':['chrom','start', 'end','Gene name'], 
                    'formats':['S64',int,int,'S64']}) # num_lines in file, -1 for header

    csvfile=open(filename,'r')
    reader=csv.reader(csvfile,delimiter='\t')
    header=reader.next() # remove header
    for i,line in enumerate(reader): # each line is a gene
        bc_gene_details[i]['chrom']=line[0]
        bc_gene_details[i]['start']=line[1]
        bc_gene_details[i]['end']=line[2]
        bc_gene_details[i]['Gene name']=line[6]

    return bc_gene_details

def export_genes_2bed(outfilename, bc_gene_details, delimiter='\t', topline=False):
    """Write each SNP as a line in a BED file"""
    outfile = open(outfilename, 'wb')
    for line in bc_gene_details:
        outfile.write(delimiter.join(map(str, line)) + '\n')
    outfile.close()




