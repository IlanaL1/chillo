import os
import numpy
from chillo.tools_config import configure_from_file
from chillo.snp_graph import SnpGraph
from chillo.io_gwis import read_table_file, init_snps_array, all_snps_from_array


def process(config, community=False, roc=True):
    """ creates a SnpGraph object and reads in the results
    from gwis-pp that contains multiple statistics
    outputs JSON file"""

    dataset = config['dataset']
    print('Processing %s' % dataset)
    gwis_file = '%s/%s_sel.txt' % (config['gwis_dir'], dataset)
    geno_file = '%s/%s' % (config['genotype_dir'], dataset)
    bim_file = '%s/%s.bim' % (config['genotype_dir'], dataset)
    json_file = '%s/%s.json' % (config['rede_dir'], dataset)

    print('Reading test statistics from gwis-pp results: %s' % gwis_file)
    pair_array = read_table_file(gwis_file)
    print('%d epistatic interaction pairs' % len(pair_array))

    gss = pair_array['fltmxGSS'].copy()
    top_pairs = numpy.argsort(gss)[-config['num_pairs']:]
    pair_array = pair_array[top_pairs]
    print('Using only top %d epistatic interaction pairs' % len(pair_array))
    
    probe_list = all_snps_from_array(pair_array)
    probe_list.sort()
    filters = pair_array.dtype.names[2:]
    print('Reading probe information from BIM file: %s' % bim_file)
    snp_array = init_snps_array(probe_list, bim_file)
    print('%d probes' % len(snp_array))

    data = SnpGraph(dataset)
    print('Initializing %s' % data)
    data.init_from_sarray(pair_array, snp_array, filters)
    data.set_adj_mat(filters[0])

    print('Finding connected subgraphs')
    data.colour_connected()
    data.sort_colours()
    data.count_edges_subgraphs()
    if community:
        print('Finding communities')
        data.find_community()
        data.count_edges_communities()
    print('Computing contingency tables from genotype file %s' % geno_file)
    data.get_contingency_tables(geno_file)

    print('Writing to %s' % json_file)
    data.export_json(json_file, roc=roc)

if __name__ == '__main__':
    import os, sys
    if len(sys.argv) != 2:
        print('Usage: python %s config.py' % sys.argv[0])
        exit(1)
    config = configure_from_file(sys.argv[1])
    process(config)
